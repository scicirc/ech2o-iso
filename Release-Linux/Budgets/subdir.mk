################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Budgets/AccountFluxes.cpp \
../Budgets/AccountRelArea.cpp \
../Budgets/AccountStorages.cpp \
../Budgets/MassBalanceError.cpp \
../Budgets/TotalFluxes.cpp \
../Budgets/TotalFluxes_d2H.cpp \
../Budgets/TotalFluxes_d18O.cpp \
../Budgets/TotalFluxes_cCl.cpp \
../Budgets/TotalFluxes_Age.cpp \
../Budgets/TotalSaturationArea.cpp \
../Budgets/TotalStorage.cpp 

OBJS += \
./Budgets/AccountFluxes.o \
./Budgets/AccountRelArea.o \
./Budgets/AccountStorages.o \
./Budgets/MassBalanceError.o \
./Budgets/TotalFluxes.o \
./Budgets/TotalFluxes_d2H.o \
./Budgets/TotalFluxes_d18O.o \
./Budgets/TotalFluxes_cCl.o \
./Budgets/TotalFluxes_Age.o \
./Budgets/TotalSaturationArea.o \
./Budgets/TotalStorage.o 

CPP_DEPS += \
./Budgets/AccountFluxes.d \
./Budgets/AccountRelArea.d \
./Budgets/AccountStorages.d \
./Budgets/MassBalanceError.d \
./Budgets/TotalFluxes.d \
./Budgets/TotalFluxes_d2H.d \
./Budgets/TotalFluxes_d18O.d \
./Budgets/TotalFluxes_cCl.d \
./Budgets/TotalFluxes_Age.d \
./Budgets/TotalSaturationArea.d \
./Budgets/TotalStorage.d 


# Each subdirectory must supply rules for building sources it contributes
Budgets/%.o: ../Budgets/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DCPU_LITTLE_ENDIAN -I"../includes" -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


