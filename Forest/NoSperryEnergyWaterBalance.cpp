/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * NoSperryEnergyWalanceBalance.cpp
 *
 *  Created on: May 12, 2021
 *      Author: Sylvain Kuppel
 */
#define ARMA_NO_DEBUG //disables armadillo bound checks for speed optimization
#define ARMA_DONT_USE_HDF5
#include <armadillo>
#include "Forest.h"

using namespace arma;

UINT4 Forest::NoSperryEnergyWaterBalance(Basin &bas, Atmosphere &atm, Control &ctrl,
					 REAL8 rootdepth, REAL8 Sold, REAL8 denom, REAL8 RWUmax,
					 REAL8 psiae, REAL8 bclambda, REAL8 ra,
					 REAL8 airTp, REAL8 airRH, REAL8 fA, REAL8 fB, REAL8 fC,
					 REAL8 LAI, REAL8 albedo, REAL8 emissivity, REAL8 BeerK,
					 REAL8 lwp_low, REAL8 lwp_high, REAL8 gamma, REAL8 leavesurfRH,
					 REAL8 &evap_a, REAL8 &transp_a,
					 REAL8 &LET, REAL8 &LE, REAL8 &H, REAL8 &netR, REAL8 &Tc, REAL8 &gc,
					 UINT4 l, REAL8 p, UINT4 s, UINT4 r, UINT4 c) {

  //some constants
  const REAL8 grav = 9.8;
  const REAL8 Vw = 18e-6; // partial molal volume of water m3 mol-1
  const REAL8 Rbar = 8.31446; // Universal gas constant in J K-1 mol-1
  //energy balance parameters
  REAL8 dt = ctrl.dt;
  REAL8 temp = 0;
  REAL8 rho_a; //density of air
  REAL8 lambda; // latent heat in ET (J.kg-1)
  REAL8 es, ea; // saturated vapor pressure
  REAL8 desdTs; // derivative of saturation vapor pressure function with respect to Ts
  REAL8 lat_heat_vap ; // latent heat of vaporization (J.kg-1)

  REAL8 ra_t; //resistance to transpiration ra_t = ra + 1/gc

  // Soil relative humidty ~ leaf stoma RH (NoSperry model)
  // used in the calculation of soil vapor pressure for latent heat exchanges
  REAL8 leafRH;
  REAL8 dleafRHdT = 0;
  REAL8 dEdlwp = 0;
  REAL8 dEdT = 0;
  REAL8 dLETdlwp =0;
  REAL8 dLETdT =0;
  REAL8 E = 0;
  REAL8 dgcdfgspsi = 0;
  REAL8 dgs_psidpsi = 0;

  /* ----------------------------------------- */

  rho_a = AirDensity(airTp); //kgm-3
  ea = SatVaporPressure(airTp) * airRH;
  lat_heat_vap = LatHeatVap(airTp); // J.kg-1

  // === System to solve the canopy-level energy balance

  int k = 0;
  
  /***
   * state variables:
   * x[0]: S - (degree of saturation at time t+1)
   * x[1]: psi_s - soil water potential
   * x[2]: Ts - Leaf temperature
   ***/
  
  colvec x(3);
  colvec deltax(3);
  colvec F(3);
  mat J = zeros<mat>(3, 3);

  //provide initial guess  for loop
  x[0] = Sold;
  x[1] = psiae / powl(x[0], bclambda);
  x[2] = airTp;
  
  // used to calculate the gc factors other than f_lwp
  // this information is contained in dgcdfgspsi and is used to calculate gc in the solution scheme below
  CalculateCanopyConduct(bas, atm, ctrl, x[1], dgcdfgspsi, s, r, c); 
  
  // Start the loop
  
  do {  
    
    // Evap - Latent heat conversion: should snow melting be factored in?
    lambda = x[2] < 0 ? lat_heat_vap + lat_heat_fus : lat_heat_vap;
    
    // Vapor pressure at saturation + dependence on canopy temperature
    es = SatVaporPressure(x[2]);
    desdTs = es * ((17.3 / (x[2] + 237.3)) - 17.3 * x[2] / (powl(x[2] + 237.3, 2)));
    // Canopy conductance and other key terms
    gc = dgcdfgspsi * Calculate_gs_lwp(x[1], lwp_high, lwp_low);
    dgs_psidpsi = (x[1] > lwp_low) || (x[1] < lwp_high) ? 0 : - 1/ (lwp_low - lwp_high);
    ra_t = ra + (1 / std::max<REAL8>(1e-13, gc));
    if(x[0]<0)
      x[0] = 0.01;
    // Leaf (stoma) relative humidity : from soil layer (leafRH~soilRH, NoSperry)
    temp = -x[1] * rho_w * grav * Vw / (Rbar*(x[2]+273.15));
    if (temp > -708.4)
      leafRH = std::min<REAL8>(1,expl(temp));
    else
      leafRH = 0;
    // Sensitivity of leafRH to canopy temperature
    dleafRHdT = leafRH *  x[1] * rho_w * grav * Vw / (Rbar*(x[2]+273.15)*(x[2]+273.15));
    /* dgcdlwp = gc == 1e-13 ? 0 : - dgcdfgspsi * lwp_c * powl(x[1]/lwp_den, lwp_c) / \
    //    (x[1] * ( powl(x[1]/lwp_den, lwp_c) + 1) * ( powl(x[1]/lwp_den, lwp_c) + 1)); */
     
    // -- Latent heat of evaporation of intercepted water, limited by maxium interception evap
    LE = LatHeatCanopy(bas, atm, leavesurfRH, ra, x[2], r, c);
    // Latent heat for root water uptake, making sure it's negative (no condensation)
    LET = LatHeatCanopy(bas, atm, leafRH, ra_t, x[2], r, c);
    // -- Sensible heat
    H = SensHeatCanopy(atm, ra, x[2], r, c);

    // Temp Et
    E = -LET / (rho_w * lambda);
    E = std::max<REAL8>(0.0,E);

    // Sensitivity of LET to L1/soil water potential
    if ((dgcdfgspsi == 0) || (dgs_psidpsi == 0))
      dLETdlwp = 0;
    else
      dLETdlwp = - rho_a * spec_heat_air * (ea - es) * dgcdfgspsi * dgs_psidpsi/ \
	(gc * gc * gamma * ra_t * ra_t); // *f1n ;
    // Sensitivity of LET to canopy temperature
    dLETdT = - rho_a * spec_heat_air / (ra_t * gamma) * (desdTs*leafRH + es*dleafRHdT); // *f1n ;
    // Conversion to E sensitivity for convenience
    dEdlwp = - dLETdlwp / (rho_w * lambda);
    dEdT = - dLETdT / (rho_w * lambda);

    // Target to be minimized
    F[0] = (x[0] - Sold) * denom * rootdepth / dt + E;
    F[1] = psiae / powl(x[0], bclambda) - x[1];
    F[2] = NetRadCanopy(atm, x[2], emissivity, albedo, BeerK, LAI, r, c) + LE + H + LET;
    // Jacobian
    // -dF[0]/dx[0], -dF[0]/dx[1] and dF[0]/dx[2]
    J(0,0) = denom * rootdepth / dt;
    J(0,1) = E == 0 ? 0 : dEdlwp;
    J(0,2) = E == 0 ? 0 : dEdT;
    // -dF[1]/dx[0] and -dF[1]/dx[1]
    J(1,0) = -bclambda * psiae * powl(x[0], -(bclambda + 1));
    J(1,1) = -1;
    // -dF[2]/dx[1] and -dF[2]/dx[2]
    J(2,1) = dLETdlwp;
    J(2,2) = fA * powl(x[2] + 273.2, 3) + fB * desdTs * leavesurfRH + fC + dLETdT;
    /* DEBUG
    if(k == 0){
      cout << "x: " << x << endl ;
      cout << "J: " << J << endl ;
      cout << "F: " << F << endl ;
      cout << "dx: " << deltax << endl ;
    }
    */
    
    // solve system
    if (!solve(deltax, J, -F)) {
      cout << "Singular Jacobian found in Newton solver for canopy balance.\n";
      //return 1;
    }
    
    x += deltax;
    
    k++;
    
  } while (norm(F, "inf") > 0.000001 && k < MAX_ITER);
  
  if (k >= MAX_ITER and norm(deltax, 2)>RNDOFFERR){
    if(l==0)
      std::cout
        << "WARNING: non-convergence in canopy energy balance for species " << s << " (p=" << p <<
        "), cell [row " << r << ", col " << c << "] - closure err: " << norm(deltax, 2)
        << endl;
    if(l>0)
      std::cout
	<< "WARNING: non-convergence in canopy energy balance for species " << s << " (p=" << p <<
	"), layer "<< l << ", cell [row " << r << ", col " << c << "] - closure err: " << norm(deltax, 2)
	<< endl;
  }
  
  /* DEBUG
     cout << "F: " << F << endl ;
  cout << "dx: " << deltax << endl ;
  cout << "x: " << x << endl ;
  cout << "J: " << J << endl ;
  */

  // If the calculated canopy temperature is lower than air temperature make it air temperature
  //Tc = std::max<REAL8>(x[2], atm.getTemperature()->matrix[r][c]);
  // -- Recalculate netR and H if Tc correction happened
  // (but not LET and LE, according to ech2o's code)
  if (x[2] < atm.getTemperature()->matrix[r][c]){
    LET = LatHeatCanopy(bas, atm, leafRH, ra_t, x[2], r, c);
    LE = LatHeatCanopy(bas, atm, leavesurfRH, ra, x[2], r, c);
    x[2] = atm.getTemperature()->matrix[r][c];
    //H = SensHeatCanopy(atm, ra, Tc, r, c);
    //netR = NetRadCanopy(atm, Tc, emissivity, albedo, BeerK, LAI, r, c);
  }

  // Evaporation rate //swap sign since outgoing evaporation is negative and
  // we accumulate it as positive. Also checks for negative evap
  evap_a = std::min<REAL8>(-evap_a, fabs(-LE / (rho_w * lambda)));
  transp_a = std::max<REAL8>(0.0, -LET / (rho_w * lambda));

  // Variables used for later budgets
  H = SensHeatCanopy(atm, ra, x[2], r, c);
  netR = NetRadCanopy(atm, x[2], emissivity, albedo, BeerK, LAI, r, c);

  // Evap - Latent heat conversion: should snow melting be factored in?
  //lambda = x[2] < 0 ? lat_heat_vap + lat_heat_fus : lat_heat_vap;

  // Make sure E is lower (LE less negative) than maximum canopy evaporation
  // (from original evap_a) without sign swap since outgoing evaporation
  // was already negative (to be corrected?)
  //LE = std::max<REAL8>(evap_a*rho_w*lambda,
  //		       std::min<REAL8>(0.0, LatHeatCanopy(bas, atm, leavesurfRH, ra, x[2], r, c)));

  /// Make sure root uptake does not pass wilting point
  // (using LET -negative-, so we take LET (less negative) > LET_RWUmax)
  // REAL8 Tp = transp_a * ctrl.dt;
  // REAL8 WaterAv = std::max<REAL8>(0.0,theta - theta_wp) * rootdepth;
  //LET = std::max<REAL8>(-RWUmax*rho_w*lambda/ctrl.dt,
  //			std::min<REAL8>(0.0, LatHeatCanopy(bas, atm, leafRH, ra_t, x[2], r, c)));

  // this chunk of code is to make sure we are not transpiring below residual moisture content
  // Probably not needed anymore since mass balance is enforced in the system of eqs.
  // solved in this function
  REAL8 Tp = transp_a * ctrl.dt;
  ///TODO: change to wilting point (not residual water content)
  if (RWUmax < Tp)
    transp_a = RWUmax / ctrl.dt;
  
  if(ctrl.sw_zPTF == 0){

    // Update canopy storage
    //DelCanStor -= evap_a * ctrl.dt; // stored for cross-species update
    
    _species[s]._Temp_c->matrix[r][c] = x[2] ;
    _species[s]._NetR_Can->matrix[r][c] = netR ; //Net radiation
    _species[s]._LatHeat_CanE->matrix[r][c] = LE ; // Latent heat of canopy evap
    _species[s]._LatHeat_CanT->matrix[r][c] = LET; // Latent heat of transpiration
    _species[s]._SensHeat_Can->matrix[r][c] = H ; //SensHeatCanopy(atm, ra, x[2], r, c);
    
    _species[s]._Einterception->matrix[r][c] = evap_a;
    _species[s]._Transpiration->matrix[r][c] = transp_a ;
    _species[s]._ET->matrix[r][c] = transp_a + evap_a;
    // then Es is added in surface routines
    _species[s]._WaterStorage->matrix[r][c] -= evap_a * ctrl.dt; //species-level
    
    // Updates canopy conductance with final values of soil potential
    CalculateCanopyConduct(bas, atm, ctrl, x[1], dgcdfgspsi, s, r, c);
    
  } else {
    
    // With vertically-variable PTF functions, gc and other flux variables
    // will be weight-summed outside, in SolveCAnopyEnergyBalance
    Tc= x[2];
    //Updates canopy conductance with final values of soil potential
    CalculateCanopyConduct(bas, atm, ctrl, x[1], dgcdfgspsi, s, r, c);
    gc = dgcdfgspsi * Calculate_gs_lwp(x[1], lwp_high, lwp_low);

  }
  
  //////////////////////////////////////////
  
  return EXIT_SUCCESS;
  }
  
