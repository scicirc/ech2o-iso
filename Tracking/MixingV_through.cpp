/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * MixingV_through.cpp
 *
 *  Created on: Jun 21, 2017
 *      Author: Sylvain Kuppel
 */

#include "Basin.h"

void Tracking::MixingV_through(Atmosphere &atm, Basin &bsn, Control &ctrl,
			    double &rain, double &irrig, double &p, int r, int c) //time step
{

  double dt = ctrl.dt;
  double pond_old = bsn.getPondingWater()->matrix[r][c];
  double d2H_in, d18O_in, cCl_in ;
  
  // This assumes that throughfall is the same as the same-day water
  // (= excess from storage capacity), i.e., there is no spill-over
  // from previously stored intercepted water
  
  // Deuterium
  if(ctrl.sw_2H){
    if(ctrl.sw_irrig)
      d2H_in = (rain*atm.getd2Hprecip()->matrix[r][c] + \
		irrig*atm.getd2Hirrig()->matrix[r][c])/(rain+irrig) ;
    else
      d2H_in = atm.getd2Hprecip()->matrix[r][c] ;

    _d2Hponding->matrix[r][c] = InputMix(pond_old, _d2Hponding->matrix[r][c],
					 (rain+irrig)*p*dt,d2H_in);

  }

  // Oxygen 18
  if(ctrl.sw_18O){
    if(ctrl.sw_irrig)
      d18O_in = (rain*atm.getd18Oprecip()->matrix[r][c] + \
		 irrig*atm.getd18Oirrig()->matrix[r][c])/(rain+irrig) ;
    else
      d18O_in = atm.getd18Oprecip()->matrix[r][c] ;

    _d18Oponding->matrix[r][c] = InputMix(pond_old, _d18Oponding->matrix[r][c],
					  (rain+irrig)*p*dt,d18O_in);
  }

  // Chloride
  if(ctrl.sw_Cl){
    if(ctrl.sw_irrig)
      cCl_in = (rain*atm.getcClprecip()->matrix[r][c] + \
		irrig*atm.getcClirrig()->matrix[r][c])/(rain+irrig) ;
    else
      cCl_in = atm.getcClprecip()->matrix[r][c] ;

    _cClponding->matrix[r][c] = InputMix(pond_old, _cClponding->matrix[r][c],
					 (rain+irrig)*p*dt,cCl_in);
  }

  // Water age
  if(ctrl.sw_Age)
    _Ageponding->matrix[r][c] = InputMix(pond_old, _Ageponding->matrix[r][c],
					 (rain+irrig)*p*dt,0.0);

}
