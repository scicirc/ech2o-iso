/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta
 *******************************************************************************/
/*
 * Report2screen.cpp
 *
 *  Created on: Aug 2, 2010
 *      Author: Marco.Maneta
 */

#include "Sativa.h"

int Report2Screen(){

  UINT4 ncell = oBasin->getSortedGrid().cells.size();
  REAL8 dx = oBasin->getCellSize();
  REAL8 area = ncell * dx * dx;

  // ==== Splash screen --------------------------------------------------------
  // For the splash screen, using mm for flux and stores
  // makes things more obvious than m3 for visual check

  printf("\nPrecipitation (mm):  %.2f \t", 1000 * oBudget->precipitation / area);
  printf("Throughfall (mm): %.2f \n", 1000 * oBudget->throughfall / area);
  if(oControl->sw_irrig)
    printf("Irrigation (mm):  %.2f \n", 1000 * oBudget->irrigation / area);
  printf("Interception (mm): %.2f \t", 1000 * oBudget->canopy /area);
  printf("Snow storage (SWE, mm): %.2f \n", 1000 * oBudget->snowpack / area);
  printf("Surface water (mm): %.2f \t", 1000 * (oBudget->channel+oBudget->ponding) / area);
  printf("Surface infiltration (mm): %.2f \n", 1000 * oBudget->infiltration / area);
  printf("Subsurface water (mm): %.2f \t", 1000 * oBudget->vadose / area);
  //printf("including RootZone water (mm): %.2f \t", 1000 * oBudget->rootzone / area);
  printf("incl. saturated zone (mm): %.2f \n", 1000 * oBudget->grndwater / area);
  printf("Total ET (mm): %.2f \t", 1000 * oBudget->evaporation / area);
  printf("Soil evap (mm): %.2f \n", 1000 * oBudget->evaporationS /area);
  printf("Evap. interception (mm): %.2f \t", 1000 * oBudget->evaporationI / area);
  printf("Transpiration (mm): %.2f \n", 1000 * oBudget->transpiration / area);
  //printf("In-domain recharge (mm): %.2f \t", 1000 * oBudget->recharge / area);
  printf("Surf. exfiltration (mm): %.2f \t", 1000 * oBudget->exfiltration / area);
  printf("Bottom drainage output (mm): %.2f \n", 1000 * oBudget->leakage /area);
  printf("Runoff to stream (mm): %.2f \t", 1000 * oBudget->srftochn / area);
  printf("GW seepage to stream (mm): %.2f \n", 1000 * oBudget->gwtochn / area);
  printf("Overland output (mm): %.2f \t", 1000 * oBudget->ovlndflow / area);
  printf("Interflow output (mm): %.2f \n", 1000 * oBudget->gwtrflow / area);
  printf("Saturated area (%): %.2f \t", oBudget->satarea*100);
  printf("Water balance error (%): %e \n", oBudget->MBErr);

  // ==== BasinSummary.txt --------------------------------------------------------
  // m3 is kept for BasinSummary.txt

  ofSummary << oBudget->precipitation << "\t";
  ofSummary << oBudget->throughfall << "\t";
  ofSummary << oBudget->canopy << "\t";
  if(oControl->sw_irrig)
    ofSummary << oBudget->irrigation << "\t";
  ofSummary << oBudget->snowpack << "\t";
  ofSummary << oBudget->snowmelt << "\t";
  ofSummary << (oBudget->channel+oBudget->ponding) << "\t";
  ofSummary << oBudget->infiltration << "\t";
  ofSummary << oBudget->vadose << "\t";
  ofSummary << oBudget->soilL1 << "\t"; // not in splash screen
  ofSummary << oBudget->soilL2 << "\t"; // not in splash screen
  ofSummary << oBudget->soilL3 << "\t"; // not in splash screen
  ofSummary << oBudget->rootzone << "\t";
  ofSummary << oBudget->grndwater << "\t";
  ofSummary << oBudget->evaporation << "\t";
  ofSummary << oBudget->evaporationI << "\t";
  ofSummary << oBudget->evaporationS << "\t";
  ofSummary << oBudget->transpiration << "\t";
  //ofSummary << oBudget->recharge << "\t";
  //ofSummary << oBudget->exfiltration << "\t";
  ofSummary << oBudget->leakage << "\t";
  ofSummary << oBudget->srftochn << "\t";
  ofSummary << oBudget->gwtochn << "\t";
  ofSummary << oBudget->ovlndflow << "\t";
  ofSummary << oBudget->gwtrflow << "\t";
  ofSummary << oBudget->satarea << "\t";
  ofSummary << oBudget->MBErr << "\n";

  // ==== Basind2HSummary.txt --------------------------------------------------------
  // -----------------------------------------------------
  if(oControl->sw_trck and oControl->sw_2H){
    ofd2HSummary << oBudget->d2Hprecip << "\t";
    ofd2HSummary << oBudget->d2Hcanopy << "\t";
    if(oControl->sw_irrig)
      ofd2HSummary << oBudget->d2Hirrig << "\t";    
    ofd2HSummary << oBudget->d2Hsnowpack << "\t";
    ofd2HSummary << oBudget->d2Hsnowmelt << "\t";
    ofd2HSummary << oBudget->d2Hchannel << "\t";
    ofd2HSummary << oBudget->d2Hinfilt << "\t";
    ofd2HSummary << oBudget->d2Hvadose << "\t";
    ofd2HSummary << oBudget->d2HsoilL1 << "\t";
    ofd2HSummary << oBudget->d2HsoilL2 << "\t";
    ofd2HSummary << oBudget->d2HsoilL3 << "\t";
    ofd2HSummary << oBudget->d2Hrootzone << "\t";
    ofd2HSummary << oBudget->d2Hgrndwater << "\t";
    ofd2HSummary << oBudget->d2Hevap << "\t";
    ofd2HSummary << oBudget->d2HevapI << "\t";
    ofd2HSummary << oBudget->d2HevapS << "\t";
    ofd2HSummary << oBudget->d2HevapT << "\t";
    ofd2HSummary << oBudget->d2Hleakage << "\t";
    ofd2HSummary << oBudget->d2Hsrftochn << "\t";
    ofd2HSummary << oBudget->d2Hgwtochn << "\t";
    ofd2HSummary << oBudget->d2HovlndOut << "\t";
    ofd2HSummary << oBudget->d2HgwOut << "\t";
    //ofd2HSummary << oBudget->d2HOut << "\t";
    //ofd2HSummary << oBudget->d2Hrecharge << "\t";
    ofd2HSummary << oBudget->d2HMBErr << "\n";
  }

  // ==== Basind18OSummary.txt --------------------------------------------------------
  // -----------------------------------------------------
  if(oControl->sw_trck and oControl->sw_18O){
    ofd18OSummary << oBudget->d18Oprecip << "\t";
    ofd18OSummary << oBudget->d18Ocanopy << "\t";
    if(oControl->sw_irrig)
      ofd18OSummary << oBudget->d18Oirrig << "\t";    
    ofd18OSummary << oBudget->d18Osnowpack << "\t";
    ofd18OSummary << oBudget->d18Osnowmelt << "\t";
    ofd18OSummary << oBudget->d18Ochannel << "\t";
    ofd18OSummary << oBudget->d18Oinfilt << "\t";
    ofd18OSummary << oBudget->d18Ovadose << "\t";
    ofd18OSummary << oBudget->d18OsoilL1 << "\t";
    ofd18OSummary << oBudget->d18OsoilL2 << "\t";
    ofd18OSummary << oBudget->d18OsoilL3 << "\t";
    ofd18OSummary << oBudget->d18Orootzone << "\t";
    ofd18OSummary << oBudget->d18Ogrndwater << "\t";
    ofd18OSummary << oBudget->d18Oevap << "\t";
    ofd18OSummary << oBudget->d18OevapI << "\t";
    ofd18OSummary << oBudget->d18OevapS << "\t";
    ofd18OSummary << oBudget->d18OevapT << "\t";
    ofd18OSummary << oBudget->d18Oleakage << "\t";
    ofd18OSummary << oBudget->d18Osrftochn << "\t";
    ofd18OSummary << oBudget->d18Ogwtochn << "\t";
    ofd18OSummary << oBudget->d18OovlndOut << "\t";
    ofd18OSummary << oBudget->d18OgwOut << "\t";
    //ofd18OSummary << oBudget->d18OOut << "\t";
    //ofd18OSummary << oBudget->d18Orecharge << "\t";
    ofd18OSummary << oBudget->d18OMBErr << "\n";
  }

  // ==== BasincClSummary.txt --------------------------------------------------------
  // -----------------------------------------------------
  if(oControl->sw_trck and oControl->sw_Cl){
    ofcClSummary << oBudget->cClprecip << "\t";
    ofcClSummary << oBudget->cClcanopy << "\t";
    if(oControl->sw_irrig)
      ofcClSummary << oBudget->cClirrig << "\t";    
    ofcClSummary << oBudget->cClsnowpack << "\t";
    ofcClSummary << oBudget->cClsnowmelt << "\t";
    ofcClSummary << oBudget->cClchannel << "\t";
    ofcClSummary << oBudget->cClinfilt << "\t";
    ofcClSummary << oBudget->cClvadose << "\t";
    ofcClSummary << oBudget->cClsoilL1 << "\t";
    ofcClSummary << oBudget->cClsoilL2 << "\t";
    ofcClSummary << oBudget->cClsoilL3 << "\t";
    ofcClSummary << oBudget->cClrootzone << "\t";
    ofcClSummary << oBudget->cClgrndwater << "\t";
    ofcClSummary << oBudget->cClevap << "\t";
    ofcClSummary << oBudget->cClevapI << "\t";
    ofcClSummary << oBudget->cClevapS << "\t";
    ofcClSummary << oBudget->cClevapT << "\t";
    ofcClSummary << oBudget->cClleakage << "\t";
    ofcClSummary << oBudget->cClsrftochn << "\t";
    ofcClSummary << oBudget->cClgwtochn << "\t";
    ofcClSummary << oBudget->cClovlndOut << "\t";
    ofcClSummary << oBudget->cClgwOut << "\t";
    //ofcClSummary << oBudget->cClOut << "\t";
    //ofcClSummary << oBudget->cClrecharge << "\t";
    ofcClSummary << oBudget->cClMBErr << "\n";

  }


  // ==== BasinAgeSummary.txt --------------------------------------------------------
  // -----------------------------------------------------
  if(oControl->sw_trck and oControl->sw_Age){
    ofAgeSummary << oBudget->Ageprecip << "\t";
    ofAgeSummary << oBudget->Agecanopy << "\t";
    if(oControl->sw_irrig)
      ofAgeSummary << oBudget->Ageirrig << "\t";    
    ofAgeSummary << oBudget->Agesnowpack << "\t";
    ofAgeSummary << oBudget->Agesnowmelt << "\t";
    ofAgeSummary << oBudget->Agechannel << "\t";
    ofAgeSummary << oBudget->Ageinfilt << "\t";
    ofAgeSummary << oBudget->Agevadose << "\t";
    ofAgeSummary << oBudget->AgesoilL1 << "\t";
    ofAgeSummary << oBudget->AgesoilL2 << "\t";
    ofAgeSummary << oBudget->AgesoilL3 << "\t";
    ofAgeSummary << oBudget->Agerootzone << "\t";
    ofAgeSummary << oBudget->Agegrndwater << "\t";
    ofAgeSummary << oBudget->Ageevap << "\t";
    ofAgeSummary << oBudget->AgeevapI << "\t";
    ofAgeSummary << oBudget->AgeevapS << "\t";
    ofAgeSummary << oBudget->AgeevapT << "\t";
    ofAgeSummary << oBudget->Ageleakage << "\t";
    ofAgeSummary << oBudget->Agesrftochn << "\t";
    ofAgeSummary << oBudget->Agegwtochn << "\t";
    ofAgeSummary << oBudget->AgeovlndOut << "\t";
    ofAgeSummary << oBudget->AgegwOut << "\t";
    //ofAgeSummary << oBudget->AgeOut << "\t";
    //ofAgeSummary << oBudget->Agerecharge << "\t";
    ofAgeSummary << oBudget->AgeMBErr << "\n";
  }

  return EXIT_SUCCESS;
}
