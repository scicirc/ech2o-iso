/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * CalculateBudgets.cpp
 *
 *  Created on: Aug 2, 2010
 *      Author: Marco.Maneta
 */

#include "Sativa.h"

int CalculateBudgets(){

  oBudget->TotalPrecipitation(oAtmosphere->getPrecipitation(), oAtmosphere);
  oBudget->TotalThroughfall(oBasin->getFluxThroughfall(), oBasin);
  if(oControl->sw_irrig)
    oBudget->TotalIrrigation(oAtmosphere->getIrrigation(), oAtmosphere);
  oBudget->TotalSnowmelt(oBasin->getFluxSnowmelt(), oBasin);
  oBudget->TotalInfiltration(oBasin->getFluxInfilt(), oBasin);
  oBudget->TotalEvaporation(oBasin->getEvaporation(), oBasin);
  oBudget->TotalEvaporationS(oBasin->getEvaporationS_all(), oBasin);
  oBudget->TotalEvaporationI(oBasin->getEvaporationI_all(), oBasin);
  oBudget->TotalTranspiration(oBasin->getTranspiration_all(), oBasin);
  oBudget->TotalRecharge(oBasin->getFluxRecharge(), oBasin);
  oBudget->TotalExfiltration(oBasin->getFluxExfilt(), oBasin);
  oBudget->TotalBedrockLeakage(oBasin->getFluxLeakage(), oBasin);
  oBudget->TotalSrftoChn(oBasin->getFluxSrftoChn(), oBasin);
  oBudget->TotalGWtoChn(oBasin->getFluxGWtoChn(), oBasin);
  oBudget->TotalOvlndFlow(oBasin->getOvlndOutput(), oBasin);
  oBudget->TotalGrndFlow(oBasin->getGwtrOutput(), oBasin);
  oBudget->TotalStorage(oBasin->getCanopyStorage(),
			oBasin->getSnowWaterEquiv(),
			oBasin->getPondingWater(), // ponding always=0, might change in later versions
			oBasin->getChannelWater(),
			//oBasin->getSoilWaterDepth(),
			oBasin->getSoilWaterDepthL1(),
			oBasin->getSoilWaterDepthL2(),
			oBasin->getSoilWaterDepthL3(),
			//oBasin->getGravityWater(),
			oBasin->getGrndWater(),
			oBasin->getProotzoneL1(),
			oBasin->getProotzoneL2(),
			oBasin->getProotzoneL3(),
			oBasin);
  oBudget->TotalSaturationArea(oBasin->getSatArea(), oBasin);

   // ---------------------------------------------------------------------------------------------

  // ################################################################################################
  // Tracking ---------------------------------------------------------------------------------------
  if(oControl->sw_trck){
    // === Deuterium ================================================================================
    if (oControl->sw_2H){
      oBudget->TotalPrecipitation_d2H(oAtmosphere->getPrecipitation(), oAtmosphere->getd2Hprecip(),
				     oAtmosphere);
      if(oControl->sw_irrig)
         oBudget->TotalIrrigation_d2H(oAtmosphere->getIrrigation(), oAtmosphere->getd2Hirrig(),
            oAtmosphere);
      oBudget->TotalEvaporationS_d2H(oBasin->getEvaporationS_all(), oTracking->getd2HevapS_sum(),
				    oBasin);
      oBudget->TotalEvaporationI_d2H(oBasin->getEvaporationI_all(), oTracking->getd2HevapI_sum(),
				    oBasin);
      oBudget->TotalTranspiration_d2H(oBasin->getTranspiration_all(), oTracking->getd2HevapT_sum(),
				     oBasin);
      oBudget->TotalBedrockLeakage_d2H(oBasin->getFluxLeakage(), oTracking->getd2Hleakage(), oBasin);
      oBudget->TotalOvlndFlow_d2H(oBasin->getOvlndOutput(), oTracking->getd2HOvlndOutput());
      oBudget->TotalGrndFlow_d2H(oBasin->getGwtrOutput(), oTracking->getd2HGwtrOutput());
      oBudget->TotalStorage_d2H(oBasin->getCanopyStorage(), oTracking->getd2Hcanopy_sum(),
			       oBasin->getSnowWaterEquiv(),  oTracking->getd2Hsnowpack(),
			       oBasin->getPondingWater(), oTracking->getd2Hponding(),
				oBasin->getChannelWater(), oTracking->getd2Hchannel(),
			       oBasin->getSoilWaterDepthL1(), oTracking->getd2Hsoil1(),
				oBasin->getProotzoneL1(),
				oBasin->getSoilWaterDepthL2(), oTracking->getd2Hsoil2(),
				oBasin->getProotzoneL2(),
				oBasin->getSoilWaterDepthL3(), oTracking->getd2Hsoil3(),
				oBasin->getProotzoneL3(),
				oBasin->getGrndWater(), oTracking->getd2HGWall(),
			       oBasin);//, oControl);


      // d2H for Basind2HSummary.txt
      oBudget->InstPrecipitation_d2H(oAtmosphere->getPrecipitation(),oAtmosphere->getd2Hprecip(),
					 oBasin);
      if(oControl->sw_irrig)
	oBudget->InstIrrigation_d2H(oAtmosphere->getIrrigation(), oAtmosphere->getd2Hirrig(),
					   oBasin);
      oBudget->InstSnowmelt_d2H(oBasin->getFluxSnowmelt(), oTracking->getd2Hsnowmelt(),
               oBasin);
      oBudget->InstInfiltration_d2H(oBasin->getFluxInfilt(), oTracking->getd2Hinfilt(),
               oBasin);
      oBudget->InstExfiltration_d2H(oBasin->getFluxExfilt(), oTracking->getd2Hexfilt(),
               oBasin);
      oBudget->InstEvaporation_d2H(oBasin->getEvaporationS_all(), oTracking->getd2HevapS_sum(),
				   oBasin->getEvaporationI_all(), oTracking->getd2HevapI_sum(),
				   oBasin->getTranspiration_all(), oTracking->getd2HevapT_sum(),
				   oBasin);
      oBudget->InstEvaporationS_d2H(oBasin->getEvaporationS_all(), oTracking->getd2HevapS_sum(),
				     oBasin);
      oBudget->InstEvaporationI_d2H(oBasin->getEvaporationI_all(), oTracking->getd2HevapI_sum(),
				     oBasin);
      oBudget->InstTranspiration_d2H(oBasin->getTranspiration_all(), oTracking->getd2HevapT_sum(),
				      oBasin);
      oBudget->InstBedrockLeakage_d2H(oBasin->getFluxLeakage(), oTracking->getd2Hleakage(),
				       oBasin);

      oBudget->InstOvlndFlow_d2H(oBasin->getOvlndOutput(), oTracking->getd2HOvlndOutput());
      oBudget->InstGrndFlow_d2H(oBasin->getGwtrOutput(), oTracking->getd2HGwtrOutput());

      oBudget->InstOut_d2H(oBasin->getEvaporationS_all(), oTracking->getd2HevapS_sum(),
			   oBasin->getEvaporationI_all(), oTracking->getd2HevapI_sum(),
			   oBasin->getTranspiration_all(), oTracking->getd2HevapT_sum(),
			   oBasin->getFluxLeakage(), oTracking->getd2Hleakage(),
			   oBasin->getOvlndOutput(), oTracking->getd2HOvlndOutput(),
			   oBasin->getGwtrOutput(), oTracking->getd2HGwtrOutput(),
			   oBasin);

      oBudget->InstSrftoChn_d2H(oBasin->getFluxSrftoChn(), oTracking->getd2HSrftoChn(), oBasin);
      oBudget->InstGWtoChn_d2H(oBasin->getFluxGWtoChn(), oTracking->getd2HGWtoChn(), oBasin);
      oBudget->InstRecharge_d2H(oBasin->getFluxRecharge(), oTracking->getd2HRecharge(), oBasin);

    }

    // === Oxygen 18 ====================================================================================
    if (oControl->sw_18O){
      oBudget->TotalPrecipitation_d18O(oAtmosphere->getPrecipitation(), oAtmosphere->getd18Oprecip(),
				     oAtmosphere);
      if(oControl->sw_irrig)
         oBudget->TotalIrrigation_d18O(oAtmosphere->getIrrigation(), oAtmosphere->getd18Oirrig(),
            oAtmosphere);
      oBudget->TotalEvaporationS_d18O(oBasin->getEvaporationS_all(), oTracking->getd18OevapS_sum(),
				    oBasin);
      oBudget->TotalEvaporationI_d18O(oBasin->getEvaporationI_all(), oTracking->getd18OevapI_sum(),
				    oBasin);
      oBudget->TotalTranspiration_d18O(oBasin->getTranspiration_all(), oTracking->getd18OevapT_sum(),
				     oBasin);
      oBudget->TotalBedrockLeakage_d18O(oBasin->getFluxLeakage(), oTracking->getd18Oleakage(), oBasin);
      oBudget->TotalOvlndFlow_d18O(oBasin->getOvlndOutput(), oTracking->getd18OOvlndOutput());
      oBudget->TotalGrndFlow_d18O(oBasin->getGwtrOutput(), oTracking->getd18OGwtrOutput());
      oBudget->TotalStorage_d18O(oBasin->getCanopyStorage(), oTracking->getd18Ocanopy_sum(),
			       oBasin->getSnowWaterEquiv(),  oTracking->getd18Osnowpack(),
			       oBasin->getPondingWater(), oTracking->getd18Oponding(),
				oBasin->getChannelWater(), oTracking->getd18Ochannel(),
			       oBasin->getSoilWaterDepthL1(), oTracking->getd18Osoil1(),
				oBasin->getProotzoneL1(),
				oBasin->getSoilWaterDepthL2(), oTracking->getd18Osoil2(),
				oBasin->getProotzoneL2(),
				oBasin->getSoilWaterDepthL3(), oTracking->getd18Osoil3(),
				oBasin->getProotzoneL3(),
				oBasin->getGrndWater(), oTracking->getd18OGWall(),
			       oBasin);//, oControl);


      // d18O for Basind18OSummary.txt
      oBudget->InstPrecipitation_d18O(oAtmosphere->getPrecipitation(),oAtmosphere->getd18Oprecip(),
					 oBasin);
      if(oControl->sw_irrig)
	oBudget->InstIrrigation_d18O(oAtmosphere->getIrrigation(), oAtmosphere->getd18Oirrig(),
					   oBasin);
      oBudget->InstSnowmelt_d18O(oBasin->getFluxSnowmelt(), oTracking->getd18Osnowmelt(),
               oBasin);
      oBudget->InstInfiltration_d18O(oBasin->getFluxInfilt(), oTracking->getd18Oinfilt(),
               oBasin);
      oBudget->InstExfiltration_d18O(oBasin->getFluxExfilt(), oTracking->getd18Oexfilt(),
               oBasin);
      oBudget->InstEvaporation_d18O(oBasin->getEvaporationS_all(), oTracking->getd18OevapS_sum(),
				   oBasin->getEvaporationI_all(), oTracking->getd18OevapI_sum(),
				   oBasin->getTranspiration_all(), oTracking->getd18OevapT_sum(),
				   oBasin);
      oBudget->InstEvaporationS_d18O(oBasin->getEvaporationS_all(), oTracking->getd18OevapS_sum(),
				     oBasin);
      oBudget->InstEvaporationI_d18O(oBasin->getEvaporationI_all(), oTracking->getd18OevapI_sum(),
				     oBasin);
      oBudget->InstTranspiration_d18O(oBasin->getTranspiration_all(), oTracking->getd18OevapT_sum(),
				      oBasin);
      oBudget->InstBedrockLeakage_d18O(oBasin->getFluxLeakage(), oTracking->getd18Oleakage(),
				       oBasin);

      oBudget->InstOvlndFlow_d18O(oBasin->getOvlndOutput(), oTracking->getd18OOvlndOutput());
      oBudget->InstGrndFlow_d18O(oBasin->getGwtrOutput(), oTracking->getd18OGwtrOutput());

      oBudget->InstOut_d18O(oBasin->getEvaporationS_all(), oTracking->getd18OevapS_sum(),
			   oBasin->getEvaporationI_all(), oTracking->getd18OevapI_sum(),
			   oBasin->getTranspiration_all(), oTracking->getd18OevapT_sum(),
			   oBasin->getFluxLeakage(), oTracking->getd18Oleakage(),
			   oBasin->getOvlndOutput(), oTracking->getd18OOvlndOutput(),
			   oBasin->getGwtrOutput(), oTracking->getd18OGwtrOutput(),
			   oBasin);

      oBudget->InstSrftoChn_d18O(oBasin->getFluxSrftoChn(), oTracking->getd18OSrftoChn(), oBasin);
      oBudget->InstGWtoChn_d18O(oBasin->getFluxGWtoChn(), oTracking->getd18OGWtoChn(), oBasin);
      oBudget->InstRecharge_d18O(oBasin->getFluxRecharge(), oTracking->getd18ORecharge(), oBasin);
    }

    // === Chloride ====================================================================================
    if (oControl->sw_Cl){
            oBudget->TotalPrecipitation_cCl(oAtmosphere->getPrecipitation(), oAtmosphere->getcClprecip(),
				     oAtmosphere);
      if(oControl->sw_irrig)
         oBudget->TotalIrrigation_cCl(oAtmosphere->getIrrigation(), oAtmosphere->getcClirrig(),
            oAtmosphere);
      oBudget->TotalBedrockLeakage_cCl(oBasin->getFluxLeakage(), oTracking->getcClleakage(), oBasin);
      oBudget->TotalOvlndFlow_cCl(oBasin->getOvlndOutput(), oTracking->getcClOvlndOutput());
      oBudget->TotalGrndFlow_cCl(oBasin->getGwtrOutput(), oTracking->getcClGwtrOutput());
      oBudget->TotalStorage_cCl(oBasin->getCanopyStorage(), oTracking->getcClcanopy_sum(),
			       oBasin->getSnowWaterEquiv(),  oTracking->getcClsnowpack(),
			       oBasin->getPondingWater(), oTracking->getcClponding(),
				oBasin->getChannelWater(), oTracking->getcClchannel(),
			       oBasin->getSoilWaterDepthL1(), oTracking->getcClsoil1(),
				oBasin->getProotzoneL1(),
				oBasin->getSoilWaterDepthL2(), oTracking->getcClsoil2(),
				oBasin->getProotzoneL2(),
				oBasin->getSoilWaterDepthL3(), oTracking->getcClsoil3(),
				oBasin->getProotzoneL3(),
				oBasin->getGrndWater(), oTracking->getcClGWall(),
			       oBasin);//, oControl);


      // cCl for BasincClSummary.txt
      oBudget->InstPrecipitation_cCl(oAtmosphere->getPrecipitation(),oAtmosphere->getcClprecip(),
					 oBasin);
      if(oControl->sw_irrig)
	oBudget->InstIrrigation_cCl(oAtmosphere->getIrrigation(), oAtmosphere->getcClirrig(),
					   oBasin);
      oBudget->InstSnowmelt_cCl(oBasin->getFluxSnowmelt(), oTracking->getcClsnowmelt(),
               oBasin);
      oBudget->InstInfiltration_cCl(oBasin->getFluxInfilt(), oTracking->getcClinfilt(),
               oBasin);
      oBudget->InstExfiltration_cCl(oBasin->getFluxExfilt(), oTracking->getcClexfilt(),
               oBasin);
      oBudget->InstBedrockLeakage_cCl(oBasin->getFluxLeakage(), oTracking->getcClleakage(),
				       oBasin);

      oBudget->InstOvlndFlow_cCl(oBasin->getOvlndOutput(), oTracking->getcClOvlndOutput());
      oBudget->InstGrndFlow_cCl(oBasin->getGwtrOutput(), oTracking->getcClGwtrOutput());

      oBudget->InstOut_cCl(oBasin->getFluxLeakage(), oTracking->getcClleakage(),
			   oBasin->getOvlndOutput(), oTracking->getcClOvlndOutput(),
			   oBasin->getGwtrOutput(), oTracking->getcClGwtrOutput(),
			   oBasin);

      oBudget->InstSrftoChn_cCl(oBasin->getFluxSrftoChn(), oTracking->getcClSrftoChn(), oBasin);
      oBudget->InstGWtoChn_cCl(oBasin->getFluxGWtoChn(), oTracking->getcClGWtoChn(), oBasin);
      oBudget->InstRecharge_cCl(oBasin->getFluxRecharge(), oTracking->getcClRecharge(), oBasin);
    }

    // === Age =====================================================================================
    if (oControl->sw_Age){
      // Age for mass balance calculation
      oBudget->TotalPrecipitation_Age();
      if(oControl->sw_irrig)
	oBudget->TotalIrrigation_Age();
      oBudget->TotalEvaporationS_Age(oBasin->getEvaporationS_all(), oTracking->getAgeevapS_sum(),
				    oBasin);
      oBudget->TotalEvaporationI_Age(oBasin->getEvaporationI_all(), oTracking->getAgeevapI_sum(),
				    oBasin);
      oBudget->TotalTranspiration_Age(oBasin->getTranspiration_all(), oTracking->getAgeevapT_sum(),
				     oBasin);
      oBudget->TotalBedrockLeakage_Age(oBasin->getFluxLeakage(), oTracking->getAgeleakage(), oBasin);
      oBudget->TotalOvlndFlow_Age(oBasin->getOvlndOutput(), oTracking->getAgeOvlndOutput());
      oBudget->TotalGrndFlow_Age(oBasin->getGwtrOutput(), oTracking->getAgeGwtrOutput());
      oBudget->TotalStorage_Age(oBasin->getCanopyStorage(), oTracking->getAgecanopy_sum(),
			       oBasin->getSnowWaterEquiv(),  oTracking->getAgesnowpack(),
			       oBasin->getPondingWater(), oTracking->getAgeponding(),
				oBasin->getChannelWater(), oTracking->getAgechannel(),
			       oBasin->getSoilWaterDepthL1(), oTracking->getAgesoil1(),
				oBasin->getProotzoneL1(),
				oBasin->getSoilWaterDepthL2(), oTracking->getAgesoil2(),
				oBasin->getProotzoneL2(),
				oBasin->getSoilWaterDepthL3(), oTracking->getAgesoil3(),
				oBasin->getProotzoneL3(),
				oBasin->getGrndWater(), oTracking->getAgeGWall(),
			       oBasin);//, oControl);


      // Age for BasinAgeSummary.txt
      oBudget->InstSnowmelt_Age(oBasin->getFluxSnowmelt(), oTracking->getAgesnowmelt(),
               oBasin);
      oBudget->InstInfiltration_Age(oBasin->getFluxInfilt(), oTracking->getAgeinfilt(),
               oBasin);
      oBudget->InstExfiltration_Age(oBasin->getFluxExfilt(), oTracking->getAgeexfilt(),
               oBasin);
      oBudget->InstEvaporation_Age(oBasin->getEvaporationS_all(), oTracking->getAgeevapS_sum(),
				   oBasin->getEvaporationI_all(), oTracking->getAgeevapI_sum(),
				   oBasin->getTranspiration_all(), oTracking->getAgeevapT_sum(),
				   oBasin);
      oBudget->InstEvaporationS_Age(oBasin->getEvaporationS_all(), oTracking->getAgeevapS_sum(),
				     oBasin);
      oBudget->InstEvaporationI_Age(oBasin->getEvaporationI_all(), oTracking->getAgeevapI_sum(),
				     oBasin);
      oBudget->InstTranspiration_Age(oBasin->getTranspiration_all(), oTracking->getAgeevapT_sum(),
				      oBasin);
      oBudget->InstBedrockLeakage_Age(oBasin->getFluxLeakage(), oTracking->getAgeleakage(),
				       oBasin);

      oBudget->InstOvlndFlow_Age(oBasin->getOvlndOutput(), oTracking->getAgeOvlndOutput());
      oBudget->InstGrndFlow_Age(oBasin->getGwtrOutput(), oTracking->getAgeGwtrOutput());

      oBudget->InstOut_Age(oBasin->getEvaporationS_all(), oTracking->getAgeevapS_sum(),
			   oBasin->getEvaporationI_all(), oTracking->getAgeevapI_sum(),
			   oBasin->getTranspiration_all(), oTracking->getAgeevapT_sum(),
			   oBasin->getFluxLeakage(), oTracking->getAgeleakage(),
			   oBasin->getOvlndOutput(), oTracking->getAgeOvlndOutput(),
			   oBasin->getGwtrOutput(), oTracking->getAgeGwtrOutput(),
			   oBasin);

      oBudget->InstSrftoChn_Age(oBasin->getFluxSrftoChn(), oTracking->getAgeSrftoChn(), oBasin);
      oBudget->InstGWtoChn_Age(oBasin->getFluxGWtoChn(), oTracking->getAgeGWtoChn(), oBasin);
      oBudget->InstRecharge_Age(oBasin->getFluxRecharge(), oTracking->getAgeRecharge(), oBasin);
    }

  } // ---------------------------------------------------------------------------------------------
  // Mass balance check
  oBudget->MassBalanceError(oControl);

  return EXIT_SUCCESS;
}
