/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * CreateWorld.cpp
 *
 *  Created on: Jul 30, 2010
 *      Author: Marco.Maneta
 */

#include "Sativa.h"

int CreateWorld(char* argv[]){

  oControl = new Control;
  cout << "Control created ok... " << "\n";
  oControl->ReadConfigFile(argv[1]);
  cout << "Config.ini read ok... " << "\n";

  oBasin = new Basin(*oControl);
  cout << "Basin created ok... " << "\n";

  oAtmosphere = new Atmosphere(*oControl);
  cout << "Atmosphere created ok... " << "\n";

  oReport = new Report(*oControl);
  cout << "Report created ok... " << "\n";

  oTracking = new Tracking(*oControl, *oBasin);
  if(oControl->sw_trck)
    cout << "Tracking created ok... " << "\n";

  oBudget = new Budget(oBasin, oControl, oTracking);
  cout << "Budget created ok... " << "\n";

  // == Basin Summary ==========================================================
  // ---
  try{
    ofSummary.open((oControl->path_ResultsFolder + "BasinSummary.txt").c_str());
    if(!ofSummary)
      throw std::ios::failure("Error opening BasinSummary.txt buffer\n");

  }catch(const std::exception &e){
    cout << e.what() << endl;
    throw;
  }
  // Headers for BasinSummary
  ofSummary << "Precip\t";
  ofSummary << "TFall\t";
  ofSummary << "Intrcp\t";
  if(oControl->sw_irrig)
    ofSummary << "Irrig\t";
  ofSummary << "Snpack\t";
  ofSummary << "Snmelt\t";
  ofSummary << "Surface\t";
  ofSummary << "Infilt\t";
  ofSummary << "Soil\t";
  ofSummary << "SoilL1\t";
  ofSummary << "SoilL2\t";
  ofSummary << "SoilL3\t";
  ofSummary << "RootW\t";
  ofSummary << "GrndW\t";
  ofSummary << "ET\t";
  ofSummary << "EvapI\t";
  ofSummary << "EvapS\t";
  ofSummary << "EvapT\t";
  //ofSummary << "Rchrge\t";
  //ofSummary << "Exfilt\t";
  ofSummary << "Leakage\t";
  ofSummary << "SrftoCh\t";
  ofSummary << "GWtoCh\t";
  ofSummary << "SrfOut\t";
  ofSummary << "GWOut\t";
  ofSummary << "SatExt\t";
  ofSummary << "MBErr\n";

  // == d2H Summary ==========================================================
  // ---
  if(oControl->sw_trck and oControl->sw_2H){
    try{
      ofd2HSummary.open((oControl->path_ResultsFolder + "Basind2HSummary.txt").c_str());
      if(!ofd2HSummary)
	throw std::ios::failure("Error opening Basind2HSummary.txt buffer\n");

    }catch(const std::exception &e){
      cout << e.what() << endl;
      throw;
    }
    // Headers for Basind2HSummary
    ofd2HSummary << "Precip\t";
    ofd2HSummary << "Intrcp\t";
    if(oControl->sw_irrig)
      ofd2HSummary << "Irrig\t";
    ofd2HSummary << "Snpack\t";
    ofd2HSummary << "Snmelt\t";
    ofd2HSummary << "Surface\t";
    ofd2HSummary << "Infilt\t";
    ofd2HSummary << "Soil\t";
    ofd2HSummary << "SoilL1\t";
    ofd2HSummary << "SoilL2\t";
    ofd2HSummary << "SoilL3\t";
    ofd2HSummary << "RootW\t";
    ofd2HSummary << "GrndW\t";
    ofd2HSummary << "ET\t";
    ofd2HSummary << "EvapI\t";
    ofd2HSummary << "EvapS\t";
    ofd2HSummary << "EvapT\t";
    //ofd2HSummary << "Rchrge\t";
    //ofd2HSummary << "Exfilt\t";
    ofd2HSummary << "Leakage\t";
    ofd2HSummary << "SrftoCh\t";
    ofd2HSummary << "GWtoCh\t";
    /*      ofd2HSummary << "RetSrf\t";
	    ofd2HSummary << "RetL1\t";
	    ofd2HSummary << "RetL2\t";*/
    ofd2HSummary << "SrfOut\t";
    ofd2HSummary << "GWOut\t";
    ofd2HSummary << "MBErr\n";    

  }

  // == d18O Summary ==========================================================
  // ---
  if(oControl->sw_trck and oControl->sw_18O){
    try{
      ofd18OSummary.open((oControl->path_ResultsFolder + "Basind18OSummary.txt").c_str());
      if(!ofd18OSummary)
	throw std::ios::failure("Error opening Basind18OSummary.txt buffer\n");

    }catch(const std::exception &e){
      cout << e.what() << endl;
      throw;
    }
    // Headers for Basind18OSummary
    ofd18OSummary << "Precip\t";
    ofd18OSummary << "Intrcp\t";
    if(oControl->sw_irrig)
      ofd18OSummary << "Irrig\t";
    ofd18OSummary << "Snpack\t";
    ofd18OSummary << "Snmelt\t";
    ofd18OSummary << "Surface\t";
    ofd18OSummary << "Infilt\t";
    ofd18OSummary << "Soil\t";
    ofd18OSummary << "SoilL1\t";
    ofd18OSummary << "SoilL2\t";
    ofd18OSummary << "SoilL3\t";
    ofd18OSummary << "RootW\t";
    ofd18OSummary << "GrndW\t";
    ofd18OSummary << "ET\t";
    ofd18OSummary << "EvapI\t";
    ofd18OSummary << "EvapS\t";
    ofd18OSummary << "EvapT\t";
    //ofd18OSummary << "Rchrge\t";
    //ofd18OSummary << "Exfilt\t";
    ofd18OSummary << "Leakage\t";
    ofd18OSummary << "SrftoCh\t";
    ofd18OSummary << "GWtoCh\t";
    /*      ofd18OSummary << "RetSrf\t";
	    ofd18OSummary << "RetL1\t";
	    ofd18OSummary << "RetL2\t";*/
    ofd18OSummary << "SrfOut\t";
    ofd18OSummary << "GWOut\t";
    ofd18OSummary << "MBErr\n";
  }

  // == cCl Summary ==========================================================
  // ---
  if(oControl->sw_trck and oControl->sw_Cl){
    try{
      ofcClSummary.open((oControl->path_ResultsFolder + "BasincClSummary.txt").c_str());
      if(!ofcClSummary)
	throw std::ios::failure("Error opening BasincClSummary.txt buffer\n");

    }catch(const std::exception &e){
      cout << e.what() << endl;
      throw;
    }
    // Headers for BasincClSummary
    ofcClSummary << "Precip\t";
    ofcClSummary << "Intrcp\t";
    if(oControl->sw_irrig)
      ofcClSummary << "Irrig\t";
    ofcClSummary << "Snpack\t";
    ofcClSummary << "Snmelt\t";
    ofcClSummary << "Surface\t";
    ofcClSummary << "Infilt\t";
    ofcClSummary << "Soil\t";
    ofcClSummary << "SoilL1\t";
    ofcClSummary << "SoilL2\t";
    ofcClSummary << "SoilL3\t";
    ofcClSummary << "RootW\t";
    ofcClSummary << "GrndW\t";
    ofcClSummary << "ET\t";
    ofcClSummary << "EvapI\t";
    ofcClSummary << "EvapS\t";
    ofcClSummary << "EvapT\t";
    //ofcClSummary << "Rchrge\t";
    //ofcClSummary << "Exfilt\t";
    ofcClSummary << "Leakage\t";
    ofcClSummary << "SrftoCh\t";
    ofcClSummary << "GWtoCh\t";
    /*      ofcClSummary << "RetSrf\t";
	    ofcClSummary << "RetL1\t";
	    ofcClSummary << "RetL2\t";*/
    ofcClSummary << "SrfOut\t";
    ofcClSummary << "GWOut\t";
    ofcClSummary << "MBErr\n";
  }

  // == Age Summary ==========================================================
  // ---
  if(oControl->sw_trck and oControl->sw_Age){
    try{
      ofAgeSummary.open((oControl->path_ResultsFolder + "BasinAgeSummary.txt").c_str());
      if(!ofAgeSummary)
	throw std::ios::failure("Error opening BasinAgeSummary.txt buffer\n");

    }catch(const std::exception &e){
      cout << e.what() << endl;
      throw;
    }
    // Headers for BasinAgeSummary
    ofAgeSummary << "Precip\t";
    ofAgeSummary << "Intrcp\t";
    if(oControl->sw_irrig)
      ofAgeSummary << "Irrig\t";
    ofAgeSummary << "Snpack\t";
    ofAgeSummary << "Snmelt\t";
    ofAgeSummary << "Surface\t";
    ofAgeSummary << "Infilt\t";
    ofAgeSummary << "Soil\t";
    ofAgeSummary << "SoilL1\t";
    ofAgeSummary << "SoilL2\t";
    ofAgeSummary << "SoilL3\t";
    ofAgeSummary << "RootW\t";
    ofAgeSummary << "GrndW\t";
    ofAgeSummary << "ET\t";
    ofAgeSummary << "EvapI\t";
    ofAgeSummary << "EvapS\t";
    ofAgeSummary << "EvapT\t";
    //ofAgeSummary << "Rchrge\t";
    //ofAgeSummary << "Exfilt\t";
    ofAgeSummary << "Leakage\t";
    ofAgeSummary << "SrftoCh\t";
    ofAgeSummary << "GWtoCh\t";
    /*      ofAgeSummary << "RetSrf\t";
	    ofAgeSummary << "RetL1\t";
	    ofAgeSummary << "RetL2\t";*/
    ofAgeSummary << "SrfOut\t";
    ofAgeSummary << "GWOut\t";
    ofAgeSummary << "MBErr\n";
  }

  return EXIT_SUCCESS;
}
