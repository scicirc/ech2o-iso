/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the 
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * TotalFluxes.cpp
 *
 *  Created on: Mar 8, 2010
 *      Author: Marco Maneta, Sylvain Kuppel
 */

#include "Budget.h"

void Budget::TotalPrecipitation(const grid* map, const Atmosphere *b)
{
  precipitation += AccountFluxes(map, b);
}
void Budget::TotalIrrigation(const grid* map, const Atmosphere *b)
{
  irrigation += AccountFluxes(map, b);
}

void Budget::TotalThroughfall(const grid* map, const Basin *b)
{
  throughfall += AccountFluxes(map, b);
}

void Budget::TotalSnowmelt(const grid* map, const Basin *b)
{
  snowmelt += AccountFluxes(map, b);
}

void Budget::TotalInfiltration(const grid *map, const Basin *b)
{
  infiltration += AccountFluxes(map, b);
}

void Budget::TotalEvaporation(const grid* map, const Basin *b)
{
	evaporation += AccountFluxes(map, b);
}

void Budget::TotalEvaporationI(const grid* map, const Basin *b)
{
  evaporationI += AccountFluxes(map, b);
}

void Budget::TotalEvaporationS(const grid* map, const Basin *b)
{
  evaporationS += AccountFluxes(map, b);
}

void Budget::TotalTranspiration(const grid* map, const Basin *b)
{
  transpiration += AccountFluxes(map, b);
}

void Budget::TotalRecharge(const grid *map, const Basin *b)
{
  recharge += AccountFluxes(map, b);
}

void Budget::TotalExfiltration(const grid *map, const Basin *b)
{
  exfiltration += AccountFluxes(map, b);
}

void Budget::TotalBedrockLeakage(const grid* map, const Basin *b)
{
  leakage += AccountFluxes(map, b);
}

void Budget::TotalSrftoChn(const grid *map, const Basin *b)
{
  srftochn += AccountFluxes(map, b);
}

void Budget::TotalGWtoChn(const grid *map, const Basin *b)
{
  gwtochn += AccountFluxes(map, b) ;
}

void Budget::TotalOvlndFlow(const vectCells *timeseries, const Basin *b)
{
  ovlndflow += AccountFluxes(timeseries, b);
}

void Budget::TotalGrndFlow(const vectCells *timeseries, const Basin *b)
{
  gwtrflow += AccountFluxes(timeseries, b);
}

