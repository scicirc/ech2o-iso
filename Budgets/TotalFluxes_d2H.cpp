/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the 
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * TotalFluxes.cpp
 *
 *  Created on: Mar 8, 2010
 *      Author: Marco Maneta, Sylvain Kuppel
 */

#include "Budget.h"

// Long-term mass fluxes, for mass balance error calculation

void Budget::TotalPrecipitation_d2H( const grid* map1,
  const grid* map2, const Atmosphere *atm)
{
  precipitation_d2H += AccountTrckFluxes(map1, map2, atm);
  //precipitation_tracers = AccountTrckFluxes(map1, map2, atm);
}
void Budget::TotalIrrigation_d2H( const grid* map1,
  const grid* map2, const Atmosphere *atm)
{
  irrigation_d2H += AccountTrckFluxes(map1, map2, atm);
}

void Budget::TotalEvaporationI_d2H(const grid* map1,
				   const grid* map2, const Basin *b)
{
  evaporationI_d2H += AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalEvaporationS_d2H(const grid* map1,
				   const grid* map2, const Basin *b)
{
  evaporationS_d2H += AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalTranspiration_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  transpiration_d2H += AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalBedrockLeakage_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  leakage_d2H += AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalOvlndFlow_d2H( const vectCells* timeseries1, const vectCells* timeseries2)
{
  ovlndflow_d2H += AccountTrckFluxes(timeseries1, timeseries2);
  //ovlndflow_tracers = AccountTrckFluxes(timeseries1, timeseries2);
}

void Budget::TotalGrndFlow_d2H( const vectCells* timeseries1, const vectCells* timeseries2)
{
  gwtrflow_d2H += AccountTrckFluxes(timeseries1, timeseries2) ;
}

// Instantaneous tracer reporting, for Basin*Summary.txt

void Budget::InstPrecipitation_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2Hprecip = AccountTrckFluxes2(map1, map2, b);
}
void Budget::InstIrrigation_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2Hirrig = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstSnowmelt_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2Hsnowmelt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstInfiltration_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2Hinfilt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstEvaporation_d2H( const grid* evapS, const grid* CevapS,
			 const grid* evapI, const grid* CevapI,
			 const grid* evapT, const grid* CevapT,
			 const Basin *b)
{
  d2Hevap = AccountTrckET(evapS, CevapS,evapI, CevapI, evapT, CevapT, b);
}

void Budget::InstEvaporationI_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2HevapI = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstEvaporationS_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2HevapS = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstTranspiration_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2HevapT = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstExfiltration_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2Hexfilt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstRecharge_d2H( const grid *map1, const grid *map2, const Basin *b)
{
  d2Hrecharge = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstBedrockLeakage_d2H( const grid* map1, const grid* map2, const Basin *b)
{
  d2Hleakage = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstSrftoChn_d2H( const grid *map1, const grid *map2, const Basin *b)
{
  d2Hsrftochn = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstGWtoChn_d2H( const grid *map1, const grid *map2, const Basin *b)
{
  d2Hgwtochn = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstOvlndFlow_d2H( const vectCells* timeseries1, const vectCells* timeseries2)
{
  d2HovlndOut = AccountTrckFluxes2(timeseries1, timeseries2);
}

void Budget::InstGrndFlow_d2H( const vectCells* timeseries1, const vectCells* timeseries2)
{
  d2HgwOut = AccountTrckFluxes2(timeseries1, timeseries2);
}

void Budget::InstOut_d2H( const grid* evapS, const grid* CevapS,
				 const grid* evapI, const grid* CevapI,
				 const grid* evapT, const grid* CevapT,
       const grid* leakage, const grid* Cleakage,
       const vectCells *OvlndOut, const vectCells *CovlndOut,
       const vectCells *GWOut, const vectCells *CgwOut,
				 const Basin *b)
{
  d2Hout = AccountTrckOut(evapS, CevapS,evapI, CevapI, evapT, CevapT,
			leakage, Cleakage, OvlndOut, CovlndOut,
			GWOut, CgwOut, b);
}
