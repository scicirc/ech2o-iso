/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the 
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * TotalFluxes.cpp
 *
 *  Created on: Mar 8, 2010
 *      Author: Marco Maneta, Sylvain Kuppel
 */

#include "Budget.h"

// Long-term mass fluxes, for mass balance error calculation

void Budget::TotalPrecipitation_Age()
{
  precipitation_Age += precipitation * dt / 86400;
  //precipitation_tracers = AccountTrckFluxes(map1, map2, atm);
}
void Budget::TotalIrrigation_Age()
{
  irrigation_Age += irrigation * dt / 86400;
}

void Budget::TotalEvaporationI_Age(const grid* map1,
				   const grid* map2, const Basin *b)
{
  evaporationI_Age += evaporationI * dt /86400 + AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalEvaporationS_Age(const grid* map1,
				   const grid* map2, const Basin *b)
{
  evaporationS_Age += evaporationS * dt /86400 + AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalTranspiration_Age( const grid* map1, const grid* map2, const Basin *b)
{
  transpiration_Age += transpiration * dt /86400 + AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalBedrockLeakage_Age( const grid* map1, const grid* map2, const Basin *b)
{
  leakage_Age += leakage * dt /86400 + AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalOvlndFlow_Age( const vectCells* timeseries1, const vectCells* timeseries2)
{
  ovlndflow_Age += ovlndflow * dt /86400 + AccountTrckFluxes(timeseries1, timeseries2);
  //ovlndflow_tracers = AccountTrckFluxes(timeseries1, timeseries2);
}

void Budget::TotalGrndFlow_Age( const vectCells* timeseries1, const vectCells* timeseries2)
{
  gwtrflow_Age += gwtrflow * dt /86400 + AccountTrckFluxes(timeseries1, timeseries2) ;
}


// Instantaneous tracer reporting, for Basin*Summary.txt

void Budget::InstSnowmelt_Age( const grid* map1, const grid* map2, const Basin *b)
{
  Agesnowmelt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstInfiltration_Age( const grid* map1, const grid* map2, const Basin *b)
{
  Ageinfilt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstEvaporation_Age( const grid* evapS, const grid* CevapS,
			 const grid* evapI, const grid* CevapI,
			 const grid* evapT, const grid* CevapT,
			 const Basin *b)
{
  Ageevap = AccountTrckET(evapS, CevapS,evapI, CevapI, evapT, CevapT, b);
}

void Budget::InstEvaporationI_Age( const grid* map1, const grid* map2, const Basin *b)
{
  AgeevapI = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstEvaporationS_Age( const grid* map1, const grid* map2, const Basin *b)
{
  AgeevapS = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstTranspiration_Age( const grid* map1, const grid* map2, const Basin *b)
{
  AgeevapT = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstExfiltration_Age( const grid* map1, const grid* map2, const Basin *b)
{
  Ageexfilt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstRecharge_Age( const grid *map1, const grid *map2, const Basin *b)
{
  Agerecharge = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstBedrockLeakage_Age( const grid* map1, const grid* map2, const Basin *b)
{
  Ageleakage = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstSrftoChn_Age( const grid *map1, const grid *map2, const Basin *b)
{
  Agesrftochn = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstGWtoChn_Age( const grid *map1, const grid *map2, const Basin *b)
{
  Agegwtochn = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstOvlndFlow_Age( const vectCells* timeseries1, const vectCells* timeseries2)
{
  AgeovlndOut = AccountTrckFluxes2(timeseries1, timeseries2);
}

void Budget::InstGrndFlow_Age( const vectCells* timeseries1, const vectCells* timeseries2)
{
  AgegwOut = AccountTrckFluxes2(timeseries1, timeseries2);
}

void Budget::InstOut_Age( const grid* evapS, const grid* CevapS,
				 const grid* evapI, const grid* CevapI,
				 const grid* evapT, const grid* CevapT,
       const grid* leakage, const grid* Cleakage,
       const vectCells *OvlndOut, const vectCells *CovlndOut,
       const vectCells *GWOut, const vectCells *CgwOut,
				 const Basin *b)
{
  Ageout = AccountTrckOut(evapS, CevapS,evapI, CevapI, evapT, CevapT,
			leakage, Cleakage, OvlndOut, CovlndOut,
			GWOut, CgwOut, b);
}
