/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the 
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta, Sylvain Kuppel
 *******************************************************************************/
/*
 * TotalFluxes.cpp
 *
 *  Created on: Mar 8, 2010
 *      Author: Marco Maneta, Sylvain Kuppel
 */

#include "Budget.h"

// Long-term mass fluxes, for mass balance error calculation

void Budget::TotalPrecipitation_cCl( const grid* map1,
  const grid* map2, const Atmosphere *atm)
{
  precipitation_cCl += AccountTrckFluxes(map1, map2, atm);
  //precipitation_tracers = AccountTrckFluxes(map1, map2, atm);
}
void Budget::TotalIrrigation_cCl( const grid* map1,
  const grid* map2, const Atmosphere *atm)
{
  irrigation_cCl += AccountTrckFluxes(map1, map2, atm);
}

void Budget::TotalBedrockLeakage_cCl( const grid* map1, const grid* map2, const Basin *b)
{
  leakage_cCl += AccountTrckFluxes(map1, map2, b);
}

void Budget::TotalOvlndFlow_cCl( const vectCells* timeseries1, const vectCells* timeseries2)
{
  ovlndflow_cCl += AccountTrckFluxes(timeseries1, timeseries2);
  //ovlndflow_tracers = AccountTrckFluxes(timeseries1, timeseries2);
}

void Budget::TotalGrndFlow_cCl( const vectCells* timeseries1, const vectCells* timeseries2)
{
  gwtrflow_cCl += AccountTrckFluxes(timeseries1, timeseries2) ;
}

// Instantaneous tracer reporting, for Basin*Summary.txt

void Budget::InstPrecipitation_cCl( const grid* map1, const grid* map2, const Basin *b)
{
  cClprecip = AccountTrckFluxes2(map1, map2, b);
}
void Budget::InstIrrigation_cCl( const grid* map1, const grid* map2, const Basin *b)
{
  cClirrig = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstSnowmelt_cCl( const grid* map1, const grid* map2, const Basin *b)
{
  cClsnowmelt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstInfiltration_cCl( const grid* map1, const grid* map2, const Basin *b)
{
  cClinfilt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstExfiltration_cCl( const grid* map1, const grid* map2, const Basin *b)
{
  cClexfilt = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstRecharge_cCl( const grid *map1, const grid *map2, const Basin *b)
{
  cClrecharge = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstBedrockLeakage_cCl( const grid* map1, const grid* map2, const Basin *b)
{
  cClleakage = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstSrftoChn_cCl( const grid *map1, const grid *map2, const Basin *b)
{
  cClsrftochn = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstGWtoChn_cCl( const grid *map1, const grid *map2, const Basin *b)
{
  cClgwtochn = AccountTrckFluxes2(map1, map2, b);
}

void Budget::InstOvlndFlow_cCl( const vectCells* timeseries1, const vectCells* timeseries2)
{
  cClovlndOut = AccountTrckFluxes2(timeseries1, timeseries2);
}

void Budget::InstGrndFlow_cCl( const vectCells* timeseries1, const vectCells* timeseries2)
{
  cClgwOut = AccountTrckFluxes2(timeseries1, timeseries2);
}

void Budget::InstOut_cCl(const grid* leakage, const grid* Cleakage,
       const vectCells *OvlndOut, const vectCells *CovlndOut,
       const vectCells *GWOut, const vectCells *CgwOut,
				 const Basin *b)
{
  cClout = AccountTrckOut(leakage, Cleakage, OvlndOut, CovlndOut,
			GWOut, CgwOut, b);
}
