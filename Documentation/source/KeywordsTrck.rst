Configuration (tracking) file keywords
======================================


Options
^^^^^^^
.. list-table::
   :header-rows: 1
   :stub-columns: 0
   :widths: 30 70
   :align: left

   * - Keyword
     - Description
   * - water\_2H
     - Boolean switch to turn deuterium tracking on (1) or off (0)
   * - water\_18O
     - Boolean switch to turn oxygen-18 tracking on (1) or off (0)
   * - water\_Cl
     - Boolean switch to turn chloride tracking on (1) or off (0)
   * - water\_Age
     - Boolean switch to turn water age tracking on (1) or off (0)
   * - water\_frac
     - Boolean switch to turn evaporative fractionation (for :math:`^2H` and :math:`^{18}O` in the topsoil on (1) or off (0)


Toggle switches
^^^^^^^^^^^^^^^
.. list-table::
   :header-rows: 1
   :stub-columns: 0
   :widths: 30 70
   :align: left

   * - Keyword
     - Description
   * - Mixing_mode
     - Discretization of the tracer mass balance equation in a given compartment and subtimestep, i.e. the representative volumes and concentration used for :math:`C\frac{dV}{dt}+V\frac{dC}{dt}=QC_{in}-QC_{out}`. 0: uses :math:`C=C^{t+1}` & :math:`V= V^{t+1}`. 1: uses :math:`C=\frac{C^t+C^{t+1}}{2}` & :math:`V=\frac{1}{2}(V^t+Q_{in}+max(0,V^t-Q_{out}))`
   * - if ``water_frac`` = 1 
     -
   * - Fractionation\_surface\_relhum
     - Relative humidity in air space of soil pores. 0: fixed to 1; 1: follows Lee and Pielke (1992); 2: follows Soderberg et al. (2012)
   * - Fractionation_turbulent_factor
     - Choice of turbulent factor *n* in kinetic fractionation. 0: *n* =1; 1: *n* depends on soil water content, following Mathieu and Bariac (1996)
   * - Fractionation_kinetic_diffusion
     - Ratio of isotopologues' diffusivities (:math:`D_{^1H^2H ^{16}O}/D_{H_2 ^{16}O}` and :math:`D_{H_2 ^{18}O}/D_{H_2 ^{16}O}`). 0: follows Merlivat (1978); 1: follows Vogt (1976); 2: empirical model by Merlivat and Jouzel (1979) adapted by Haese et al. (2013)


Two-pore domain
^^^^^^^^^^^^^^^
.. list-table:: optional map file must be placed in ``Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - water_two-pore_domain
     - boolean switch     
     - :math:`[-]`
     - Turns on (1) or off (0) two-pore domain conceptualization of subsurface water mixing (first two hydrological layers)
   * - MobileWater_Transition_Head
     - Map file name
     - meters of head
     - if water_two-pore_domain = 1, map of map of pressure head delimiting the two domains: mobile water and tightly-bound


Climate information for tracers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: Binary files must be placed in ``Clim_Maps_Folder``. Only necessary if the corresponding tracking switches (``water_2H``, ``water_18O`` and/or ``water_Cl``) are set to 1. 
   :header-rows: 1
   :stub-columns: 0
   :widths: 15 30 10 45
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - d2H_precip
     - Binary climate file name
     - ‰
     - Deuterium signature of precipitation
   * - d18O_precip
     - Binary climate file name
     - ‰ 
     - Oxygen 18 signature of precipitation
   * - cCl_precip
     - Binary climate file name
     - :math:`mg\ L^{-1}`
     - Chloride concentration of precipitation

.. list-table:: if ``Irrigation_Input`` is set to 1
   :header-rows: 1
   :stub-columns: 0
   :widths: 15 30 10 45
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - d2H_irrig
     - Binary climate file name
     - ‰
     - Deuterium signature of irrigation
   * - d18O_irrig
     - Binary climate file name
     - ‰ 
     - Oxygen 18 signature of irrigation
   * - cCl_irrig
     - Binary climate file name
     - :math:`mg\ L^{-1}`
     - Chloride concentration of irrigation


Initial conditions for water tracers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: Files must be located in ``Maps_Folder``. Only necessary if the corresponding tracking switches (``water_2H``, ``water_18O``, ``water_Cl``, ``water_Age``) are set to 1. **Important** : Below, *trc* is either *d2H*, *d18O*, *cCl* or *Age*
   :header-rows: 1
   :stub-columns: 0
   :widths: 15 20 30 35
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - init\_\ *trc*\ _snowpack
     - Map file name
     - ‰, mg/L, or days
     - Initial snowpack signature
   * - init\_\ *trc*\ _surface
     - Map file name
     - ‰, mg/L, or days
     - Initial surface (channel and ponding) signature
   * - init\_\ *trc*\ _soil1
     - Map file name
     - ‰, mg/L, or days
     - Initial signature in hydrological layer 1
   * - init\_\ *trc*\ _soil2
     - Map file name
     - ‰, mg/L, or days
     - Initial signature in hydrological layer 2
   * - init\_\ *trc*\ _soil3
     - Map file name
     - ‰, mg/L, or days
     - Initial signature in hydrological layer 3


Simulation outputs: map report switches
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: Only relevant if the corresponding tracking switches (``water_2H``, ``water_18O``, ``water_Cl``, ``water_Age``) are set to 1. **Important** : Below, *trc* is either *d2H*, *d18O*, *cCl* or *Age*
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 20 30 25
   :align: left

   * - Keyword
     - Unit
     - Description
     - File root (*d2H*, *d18O*, *cCl*, *Age*)
   * - Rep\_\ *trc*\ precip
     - ‰, mg/L
     - Precipitation signature (climate input)
     - dHpcp, dOpcp, Clpcp
   * - Rep\_\ *trc*\ irrig
     - ‰, mg/L, or days
     - Irrigation signature (climate input)
     - dHirg, dOirg, Clirg
   * - Rep\_\ *trc*\ canopy
     - ‰, mg/L, or days
     - Signature of intercepted water storage of species :math:`k`
     - dHcnp\ :math:`k`\_, dOcnp\ :math:`k`\_, ClCs\ :math:`k`\_, AgeCs\ :math:`k`\_
   * - Rep\_\ *trc*\ canopy_sum
     - ‰, mg/L, or days
     - Signature of intercepted water storage, summed over species fractions
     - dHcnp, dOcnp, Clcnp, Agecnp
   * - Rep\_\ *trc*\ snowpack
     - ‰, mg/L, or days
     - Snowpack signature
     - dHsnw, dOsnw, Clsnw, Agesnw
   * - Rep\_\ *trc*\ snowmelt
     - ‰, mg/L, or days
     - Snowmelt signature
     - dHsnwm, dOsnwm, Clsnwm, Agesnwm
   * - Rep\_\ *trc*\ ponding
     - ‰, mg/L, or days
     - Surface ponding signature
     - dHpnd, dOpnd, Clpnd, Agepnd 
   * - Rep\_\ *trc*\ channel
     - ‰, mg/L, or days
     - Stream water signature
     - dHchn, dOchn, Clchn, Agechn
   * - Rep\_\ *trc*\ infilt
     - ‰, mg/L, or days
     - Water signature of surface infiltration
     - dHinf, dOinf, Clinf, Ageinf
   * - Rep\_\ *trc*\ soil1
     - ‰, mg/L, or days
     - Water signature in 1st hydrological layer
     - dHsL1, dOsL1, ClsL1, AgesL1
   * - Rep\_\ *trc*\ soil2
     - ‰, mg/L, or days
     - Water signature in 2nd hydrological layer
     - dHsL2, dOsL2, ClsL2, AgesL2
   * - Rep\_\ *trc*\ soil3
     - ‰, mg/L, or days
     - Water signature in 3rd hydrological layer
     - dHsL3, dOsL3, ClsL3, AgesL3
   * - Rep\_\ *trc*\ soilUp
     - ‰, mg/L, or days
     - Storage-weighted average water signature in 2 uppermost hydrological layers
     - dHsUp, dOsUp, ClsUp, AgesUp
   * - Rep\_\ *trc*\ soilAv
     - ‰, mg/L, or days
     - Storage-weighted average water signature in the subsurface layers
     - dHsAv, dOsAv, ClsAv, AgesAv
   * - Rep\_\ *trc*\ groundwater
     - ‰, mg/L, or days
     - Signature of water above field capacity in subsurface layers (storage-weighted)
     - dHgw, dOgw, Agegw, Clgw
   * - Rep\_\ *trc*\ watertable
     - ‰, mg/L, or days
     - Signature of water in the shallowest layer with storage above field capacity
     - dHgwt, dOgwt, Agegwt, Clgwt
   * - Rep\_\ *trc*\ evapS
     - ‰ or days
     - Signature of water directly evaporated from the soil under species :math:`k`
     - dHeS\ :math:`k`\_, dOeS\ :math:`k`\_, CleS\ :math:`k`\_, AgeS\ :math:`k`\_
   * - Rep\_\ *trc*\ evapS_sum
     - ‰ or days
     - Signature of water directly evaporated from the soil, averaged over species fractions (including bare soil)
     - dHeS, dOeS, CleS, AgeeS
   * - Rep\_\ *trc*\ evapI
     - ‰ or days
     - Signature of evaporated canopy interception in species :math:`k`
     - dHeI\ :math:`k`\_, dOeI\ :math:`k`\_, CleI\ :math:`k`\_, AgeI\ :math:`k`\_
   * - Rep\_\ *trc*\ evapI_sum
     - ‰ or days
     - Signature of evaporated canopy interception, averaged over species fractions
     - dHeI, dOeI, CleI, AgeeI
   * - Rep\_\ *trc*\ evapT
     - ‰ or days
     - Signature of root-uptaken water in species :math:`k`
     - dHeT\ :math:`k`\_, dOeT\ :math:`k`\_, CleT\ :math:`k`\_, AgeT\ :math:`k`\_
   * - Rep\_\ *trc*\ evapT_sum
     - ‰ or days
     - Signature of root-uptaken water, summed over species fractions
     - dHeT, dOeT, CleT, AgeeT
   * - Rep\_\ *trc*\ GWtoChn
     - ‰, mg/L, or days
     - Signature of groundwater seepage to stream
     - dHGWQ, dOGWQ, ClGWQ, AgeGWQ
   * - Rep\_\ *trc*\ SrftoChn
     - ‰, mg/L, or days
     - Signature of runoff contribution to stream
     - dHSrfQ, dOSrfQ, ClSrfQ, AgeSrfQ
   * - If *water_two-pore_domain* set to 1
     - 
     -
     - 
   * - Rep\_\ *trc*\ soil1_MobileWater
     - ‰, mg/L, or days
     - Signature of mobile water in 1st hydrological layer
     - dHMW1, dOMW1, ClMW1, AgeMW1
   * - Rep\_\ *trc*\ soil2_MobileWater
     - ‰, mg/L, or days
     - Signature of mobile water in 2nd hydrological layer
     - dHMW2, dOMW2, ClMW2, AgeMW2
   * - Rep\_\ *trc*\ soil1_TightlyBound
     - ‰, mg/L, or days
     - Signature of tightly-bound water in 1st hydrological layer
     - dHTB1, dOTB1, ClTB1, AgeTB1
   * - Rep\_\ *trc*\ soil2_TightlyBound
     - ‰, mg/L, or days
     - Signature of tightly-bound water in 2nd hydrological layer
     - dHTB2, dOTB2, ClTB2, AgeTB2


Simulation outputs: time series report switches
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: Written outputs file are time series tables at cells identified in ``TS_mask`` (see main configuration file). In the output files, the order of columns might differ from that of locations IDs specified in ``TS_mask``, please check the output files' header. Only relevant if the corresponding tracking switches are set to 1 (``water_2H``, ``water_18O``, ``water_Cl``, ``water_Age``). **Important** : Below, *trc* is either *d2H*, *d18O*, *cCl* or *Age*
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 20 30 25
   :align: left

   * - Keyword
     - Unit
     - Description
     - File name
   * - Ts\_\ *trc*\ precip
     - ‰, mg/L
     - Precipitation signature (climate input)
     - *trc*\ \_precip.tab
   * - Ts\_\ *trc*\ irrig
     - ‰, mg/L, or days
     - Irrigation signature (climate input)
     - *trc*\ \_irrig.tab
   * - Ts\_\ *trc*\ canopy
     - ‰, mg/L, or days
     - Signature of intercepted water storage of species :math:`k`
     - *trc*\ \_canopy\_\ :math:`k`\ .tab
   * - Ts\_\ *trc*\ canopy_sum
     - ‰, mg/L, or days
     - Signature of intercepted water storage, summed over species fractions
     - *trc*\ \_canopy.tab
   * - Ts\_\ *trc*\ snowpack
     - ‰, mg/L, or days
     - Snowpack signature
     - *trc*\ \_snowpack.tab
   * - Ts\_\ *trc*\ snowmelt
     - ‰, mg/L, or days
     - Snowmelt signature
     - *trc*\ \_snowmelt.tab
   * - Ts\_\ *trc*\ ponding
     - ‰, mg/L, or days
     - Surface ponding signature
     - *trc*\ \_ponding.tab
   * - Ts\_\ *trc*\ channel
     - ‰, mg/L, or days
     - Stream water signature
     - *trc*\ \_channel.tab
   * - Ts\_\ *trc*\ infilt
     - ‰, mg/L, or days
     - Water signature of surface infiltration
     - *trc*\ \_infilt.tab
   * - Ts\_\ *trc*\ soil1
     - ‰, mg/L, or days
     - Water signature in 1st hydrological layer
     - *trc*\ \_soilL1.tab
   * - Ts\_\ *trc*\ soil2
     - ‰, mg/L, or days
     - Water signature in 2nd hydrological layer
     - *trc*\ \_soilL2.tab
   * - Ts\_\ *trc*\ soil3
     - ‰, mg/L, or days
     - Water signature in 3rd hydrological layer
     - *trc*\ \_soilL3.tab
   * - Ts\_\ *trc*\ soilUp
     - ‰, mg/L, or days
     - Storage-weighted average water signature in 2 uppermost hydrological layers
     - *trc*\ \_soilUp.tab
   * - Ts\_\ *trc*\ soilAv
     - ‰, mg/L, or days
     - Storage-weighted average water signature in the subsurface layers
     - *trc*\ \_soilAv.tab
   * - Ts\_\ *trc*\ groundwater
     - ‰, mg/L, or days
     - Signature of water above field capacity in subsurface layers (storage-weighted)
     - *trc*\ \_groundwater.tab
   * - Ts\_\ *trc*\ watertable
     - ‰, mg/L, or days
     - Signature of water in the shallowest layer with storage above field capacity
     - *trc*\ \_watertable.tab
   * - Ts\_\ *trc*\ evapS
     - ‰ or days
     - Signature of water directly evaporated from the soil under species :math:`k`
     - *trc*\ evapS\_\ :math:`k`\ .tab
   * - Ts\_\ *trc*\ evapS_sum
     - ‰ or days
     - Signature of water directly evaporated from the soil, averaged over species fractions (including bare soil)
     - *trc*\ \_evapS.tab
   * - Ts\_\ *trc*\ evapS.tab
     - ‰ or days
     - Signature of evaporated canopy interception in species :math:`k`
     - *trc*\ evapI\_\ :math:`k`\ .tab
   * - Ts\_\ *trc*\ evapI_sum
     - ‰ or days
     - Signature of evaporated canopy interception, averaged over species fractions
     - *trc*\ \_evapI.tab
   * - Ts\_\ *trc*\ evapT
     - ‰ or days
     - Signature of root-uptaken water in species :math:`k`
     - *trc*\ evapT\_\ :math:`k`\ .tab
   * - Ts\_\ *trc*\ evapT_sum
     - ‰ or days
     - Signature of root-uptaken water, summed over species fractions
     - *trc*\ \_evapT.tab
   * - Ts\_\ *trc*\ GWtoChn
     - ‰, mg/L, or days
     - Signature of groundwater seepage to stream
     - *trc*\ \_GWtoChn.tab
   * - Ts\_\ *trc*\ SrftoChn
     - ‰, mg/L, or days
     - Signature of runoff contribution to stream
     - *trc*\ \_SrftoChn.tab
   * - If *water_two-pore_domain* set to 1
     - 
     -
     - 
   * - Ts\_\ *trc*\ soil1_MobileWater
     - ‰, mg/L, or days
     - Signature of mobile water in 1st hydrological layer
     - *trc*\ \_MW1.tab
   * - Ts\_\ *trc*\ soil2_MobileWater
     - ‰, mg/L, or days
     - Signature of mobile water in 2nd hydrological layer
     - *trc*\ \_MW2.tab
   * - Ts\_\ *trc*\ soil1_TightlyBound
     - ‰, mg/L, or days
     - Signature of tightly-bound water in 1st hydrological layer
     - *trc*\ \_TB1.tab
   * - Ts\_\ *trc*\ soil2_TightlyBound
     - ‰, mg/L, or days
     - Signature of tightly-bound water in 2nd hydrological layer
     - *trc*\ \_TB2.tab