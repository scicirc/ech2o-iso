Configuration (main) file keywords
==================================

Path definitions
^^^^^^^^^^^^^^^^
.. list-table:: 
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 20 55
   :align: left

   * - Keyword
     - Type
     - Description
   * - Maps_Folder
     - System Path
     - Path to folder with land surface information
   * - Clim_Maps_Folder
     - System Path
     - Path to folder with Climate information
   * - Output_Folder
     - System Path
     - Path to folder where outputs will be written
     

Tracking
^^^^^^^^
.. list-table::
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 20 55
   :align: left

   * - Keyword
     - Type
     - Description
   * - Tracking
     - option
     - Boolean switch to turn water tracking (isotopes and/or ages) on (1) or off (0) 
   * - TrackingConfig
     - System Path
     - Location and name of the tracking configuration file
   

Options
^^^^^^^
.. list-table:: 
   :header-rows: 1
   :stub-columns: 0
   :widths: 30 70
   :align: left

   * - Keyword
     - Description
   * - MapTypes
     - Format of maps, in this version it is *csf* (PCRaster)
   * - Species_State_Variable_Input_Method
     - Specifies the input format of the vegetation state variables. Options are *table* or *maps*
   * - Reinfiltration
     - Boolean switch: reinfiltration during lateral routing on (1) or off (0)
   * - Channel
     - Boolean switch: channel routing and specific processes on (1) or off (0) 
   * - Channel_infiltration
     - Boolean switch: channel bed infiltration on (1) or off (0)
   * - Lateral_Exit
     - Boolean switch: lateral output (surface and subsurface) at the drainage outlet(s) (= 1, usual case) or not (= 0)
   * - Hydraulic_Conductivity_profile
     - Switches between different ways to prescribe horizontal saturated hydraulic conductivity maps. 0: vertically uniform; 1: exponentially-decreasing (surface value and shape factor maps); 2: one map for each subsurface layer. Corresponding maps are defined in a later section. 
   * - Porosity_profile
     - Switches between different ways to prescribe total porosity maps. 0: vertically uniform; 1: exponentially-decreasing (surface value and shape factor maps); 2: one map for each subsurface layer. Corresponding maps are defined in a later section.
   * - Aerodyn_resist_opt
     - Switches between different aerodynamic resistance formulations. 0: Penman; 1: Thom and Oliver (1977)
   * - Soil_resistance_opt
     - Switches between different soil resistance formulations. 0: No resistance; 1: Passerat de Silans et al. (1989); 2: Sellers et al. (1992); 3: Sakaguchi and Zeng (2009)
     

Time controls
^^^^^^^^^^^^^
.. list-table::
   :header-rows: 1
   :stub-columns: 0
   :widths: 20 10 10 60
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Simul_start
     - Integer
     - Seconds
     - Time of simulation start. In the current version this value must be 0
   * - Simul_end
     - Integer
     - Seconds
     - Time when simulation ends in seconds. This value indicates the total simulated time
   * - Simul_tstep
     - Integer
     - Seconds
     - Size of the integration time step
   * - Clim_input_tstep
     - Integer
     - Seconds
     - Time step of climate forcing. Typically it is the same as *Simul_tstep* but can be larger (i.e. climate inputs are daily but we are using an hourly integration time step). *Clim_input_tstep* cannot be smaller than *Simul_tstep*
   * - Report_interval
     - Integer
     - Seconds
     - Intervals between time series outputs. *Report_interval* cannot be smaller than *Simul_tstep*, typically it is equal to *Simul\_tstep* (but can be larger)
   * - ReportMap_interval
     - Integer
     - Seconds
     - Intervals between maps outputs. *ReportMap_interval* cannot be smaller than *Simul_tstep*, but can be larger to save disk space and I/O time
   * - ReportMap_starttime
     - Integer
     - Seconds
     - First time step for maps outputs (e.g. after spinup time), from which *ReportMap_interval* is counted


Vegetation dynamics
^^^^^^^^^^^^^^^^^^^
.. list-table:: optional binary file(s) must be placed in ``Clim_Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 10 10 55
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Vegetation_dynamics
     - option
     - :math:`[-]`
     - Switches between different approaches to vegetation dynamics: 0 assumes constant LAI (equal to initial value); 1 turns on vegetation allocation and growth module to calculate LAI; and 2 allows for externally forcing LAI.
   * - TimeSeries_LAI
     - Binary file
     - :math:`m^2.m^{-2}`
     - if Vegetation\_dynamics = 2, template name of binary files giving LAI dynamics, one for each species. Files name format: template + '_' + species # + '.bin'


Climate information
^^^^^^^^^^^^^^^^^^^
.. list-table:: maps and binary files must be placed in ``Clim_Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Precipitation
     - Binary climate file
     - :math:`ms^{-1}`
     - Precipitation input
   * - AirTemperature
     - Binary climate file
     - :math:`^{\circ}C`
     - Average air temperature
   * - MaxAirTemp
     - Binary climate file
     - :math:`^{\circ}C`
     - Maximum air temperature (for snow melt)
   * - MinAirTemp
     - Binary climate file
     - :math:`^{\circ}C`
     - Minimum air temperature (for snow melt)
   * - RelativeHumidity
     - Binary climate file
     - :math:`kPa~kPa^{-1}`
     - Relative air humidity
   * - WindSpeed
     - Binary climate file
     - :math:`ms^{-1}`
     - Wind speed
   * - IncomingLongWave
     - Binary climate file
     - :math:`Wm^{-2}`
     - Incoming long wave radiation
   * - IncomingShortWave
     - Binary climate file
     - :math:`Wm^{-2}`
     - Incoming solar radiation
   * - ClimateZones
     - Map file name
     - :math:`[-]`
     - Map identifying the climate zones
   * - Isohyet\_map
     - Map file name
     - :math:`[-]`
     - This map redistributes precipitation within the domain (including climate zones), using multiplication factors in each pixel. A map containing 1 over the domain does not modify the precipitation inputs


Irrigation
^^^^^^^^^^
.. list-table:: optional binary file must be placed in ``Clim_Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Irrigation_Input
     - option
     - :math:`[-]`
     - Switch to activate (1) or deactivate (0) irrigation inputs
   * - Irrigation
     - Binary file
     - :math:`ms^{-1}`
     - if Irrigation\_Input = 1, irrigation inputs


Spatial domain geometry
^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: files must be located in ``Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - DEM
     - Map file name
     - :math:`m`
     - Digital elevation model. It also defines the lateral extent and resolution of the simulation domain
   * - Slope
     - Map file name
     - :math:`m m^{-1}`
     - Local terrain slope. Rise over run
   * - local_drain_direc
     - Map file name
     - :math:`[-]`
     - Drain network: D8 steepest descent local drainage direction


S(ubs)urface parameters
^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: files must be located in ``Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Soil\_depth
     - Map file name
     - :math:`m`
     - Total domain depth
   * - Depth\_soil\_layer\_1
     - Map file name
     - :math:`m`
     - Depth of first hydrological layer
   * - Depth\_soil\_layer\_2
     - Map file name
     - :math:`m`
     - Depth of 2nd hydrological layer
   * - Vert\_Horz\_Anis\_ratio
     - Map file name
     - :math:`[-]`
     - Ratio of vertical to horizontal hydraulic conductivity
   * - Soil\_bedrock\_leakance
     - Map file name
     - :math:`[-]`
     - Factor between 0 and 1 setting the vertical hydraulic conductivity at the bottom of simulation domain (in fraction of Kv in layer 3)
   * - **if Hydraulic\_Conductivity\_profile = 0**
     -
     -
     -
   * - Horiz\_Hydraulic\_Conductivity
     - Map file name
     - :math:`ms^{-1}`
     - Saturated horizontal hydraulic conductivity
   * - **if Hydraulic_Conductivity_profile=1**
     - 
     - 
     - 
   * - Horiz\_Hydraulic\_Conductivity
     - Map file name
     - :math:`ms^{-1}`
     - Surface's saturated horizontal hydraulic conductivity
   * - Horiz\_Hydraulic\_Conductivity_Profile_Coeff
     - Map file name
     - :math:`m`
     - Exponential depth decay shape factor for sat. horizontal hydraulic conductivity
   * - **if Hydraulic\_Conductivity\_profile = 2**
     - 
     - 
     - 
   * - Horiz\_Hydraulic\_Conductivity
     - Map file name
     - :math:`ms^{-1}`
     - Layer 1's saturated horizontal hydraulic conductivity
   * - Horiz\_Hydraulic\_Conductivity_Layer2
     - Map file name
     - :math:`ms^{-1}`
     - Layer 2's saturated horizontal hydraulic conductivity
   * - Horiz\_Hydraulic\_Conductivity_Layer3
     - Map file name
     - :math:`ms^{-1}`
     - Layer 3's saturated horizontal hydraulic conductivity
   * - **if Porosity\_profile = 0**
     -
     -
     -
   * - Porosity
     - Map file name
     - :math:`[-]`
     - Total porosity
   * - Residual_soil_moisture
     - Map file name
     - :math:`m^{3}~m^{-3}`
     - Minimum allowed volumetric water content 
   * - **if Porosity\_profile = 1**
     - 
     - 
     - 
   * - Porosity
     - Map file name
     - :math:`[-]`
     - Surface-level total porosity
   * - Porosity\_Profile\_Coeff
     - Map file name
     - :math:`[-]`
     - Exponential depth decay shape factor for porosity
   * - Residual\_soil\_moisture
     - Map file name
     - :math:`m^{3}~m^{-3}`
     - Minimum allowed volumetric water content
   * - **if Porosity\_profile = 2**
     - 
     - 
     - 
   * - Porosity
     - Map file name
     - :math:`[-]`
     - Layer 1's total porosity
   * - Porosity\_Layer2
     - Map file name
     - :math:`[-]`
     - Layer 2's total porosity
   * - Porosity\_Layer3
     - Map file name
     - :math:`[-]`
     - Layer 3's total porosity
   * - Residual\_soil\_moisture
     - Map file name
     - :math:`m^{3}~m^{-3}`
     - Layer 1's minimum allowed volumetric water content
   * - Residual\_soil\_moisture_Layer2
     - Map file name
     - :math:`m^{3}~m^{-3}`
     - Layer 2's minimum allowed volumetric water content
   * - Residual\_soil\_moisture_Layer3
     - Map file name
     - :math:`m^{3}~m^{-3}`
     - Layer 3's minimum allowed volumetric water content
   * -
     - 
     - 
     - 
   * - Air\_entry\_pressure
     - Map file name
     - :math:`m`
     - Soil air entry pressure
   * - Brooks\_Corey\_lambda
     - Map file name
     - :math:`[-]`
     - Pore size distribution
   * - 
     - 
     - 
     - 
   * - Veget\_water\_use\_param1
     - Map file name
     - :math:`m`
     - Vegetation water use parameter from Landsberg & Waring (1997)
   * - Veget\_water\_use\_param1
     - Map file name
     - :math:`m`
     - Vegetation water use parameter from Landsberg & Waring (1997)
   * - Albedo
     - Map file name
     - :math:`[-]`
     - Surface albedo
   * - Surface\_emissivity
     - Map file name
     - :math:`[-]`
     - Surface emissivity/absorptivity
   * - Terrain\_Random\_Roughness
     - Map file name
     - :math:`m`
     - Local surface roughness
   * - Dry\_Soil\_Heat\_Capacity
     - Map file name
     - :math:`Jm^{-3}K^{-1}`
     - Heat capacity of soil solid particles 
   * - Dry\_Soil\_Therm\_Cond
     - Map file name
     - :math:`Wm^{-1}K^{-1}`
     - Thermal conductivity of soil solid particles
   * - Damping\_depth
     - Map file name
     - :math:`m`
     - Depth of bottom of second soil thermal layer 


Snow parameters
^^^^^^^^^^^^^^^
.. list-table:: files must be located in ``Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Snow\_rain\_temp\_threshold
     - Map file name
     - :math:`^{\circ}C`
     - Air temperature threshold for snow/rain transition
   * - Snow\_Melt\_Coeff
     - Map file name
     - :math:`m^{\circ}C^{-1}`
     - Snowmelt coefficient factor


Channel parameters
^^^^^^^^^^^^^^^^^^
.. list-table:: files must be located in ``Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - channel\_width
     - Map file name
     - :math:`m`
     - Mask with width of channel network. Pixels with no channel must be <= 0. Positive numbers indicate the width of the channel in the pixel
   * - channel\_gw\_transfer\_param
     - Map file name
     - :math:`m^{-1}`
     - Coefficient controlling transfers of water from the subsurface system (here only layer 3) to the channel
   * - mannings\_n
     - Map file name
     - :math:`sm^{-1/3}`
     - Manning's n roughness coefficient in the channel


Initial (sub)surface states
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: files must be located in ``Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Streamflow 
     - Map file name
     - :math:`m^3 s^{-1}`
     - Stream discharge
   * - snow\_water\_equivalent 
     - Map file name
     - :math:`m`
     - Snow water equivalent
   * - Soil\_moisture\_1
     - Map file name
     - :math:`m^3 m^{-3}`
     - Volumetric water content (hydro layer 1)
   * - Soil\_moisture\_2
     - Map file name
     - :math:`m^3 m^{-3}`
     - Volumetric water content (hydro layer 2)
   * - Soil\_moisture\_3
     - Map file name
     - :math:`m^3 m^{-3}`
     - Volumetric water content (hydro layer 3)
   * - Soil\_temperature
     - Map file name
     - :math:`^{\circ}C`
     - Temperature atop the top thermal layer
   * - Temp\_at\_damp\_depth
     - Map file name
     - :math:`^{\circ}C`
     - Average temp. of bottom thermal layer


Vegetation parameters
^^^^^^^^^^^^^^^^^^^^^
.. list-table:: files must be located in ``Maps_Folder``
   :header-rows: 1
   :stub-columns: 0
   :widths: 25 25 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - ForestPatches
     - Map file name
     - integers
     - Map identifying land cover categories (patches)
   * - Number\_of\_Species
     - Integer
     - :math:`[-]`
     - Number of species included in the simulation
   * - Species\_Parameters
     - Parameter table
     - :math:`[-]`
     - Table containing the parameters values (up to 40) for each simulated species. Only the first ``Number_of_Species`` lines are read.


Initial vegetation states \*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: \* if ``Species_State_Variable_Input_Method`` = ``tables``
   :header-rows: 1
   :stub-columns: 0
   :widths: 30 20 10 40
   :align: left

   * - Keyword
     - Type
     - Unit
     - Description
   * - Species\_Proportion\_Table
     - Variable table
     - :math:`m^{2} m^{-2}`
     - Table with initial fraction (<=1) of covered area (canopy cover) for each species 
   * - Species\_LAI\_Table
     - Variable table
     - :math:`m^{2} m^{-2}`
     - Table with initial leaf area index for each species. Overriden if Vegetation_dynamics= 2.
   * - `Below variables are mostly for biomass allocation routines`
     - 
     - 
     - 
   * - Species\_StemDensity\_Table
     - Variable table
     - :math:`trees.m^{-2}`
     - Table with initial stem density for each species. Values only used for ligneous species 
   * - Species\_AGE\_Table
     - Variable table
     - :math:`years`
     - Table with initial average age for each species
   * - Species\_BasalArea\_Table
     - Variable table
     - :math:`m^{2}`
     - Table with initial total basal area per species. 
   * - Species\_Height\_table
     - Variable table
     - :math:`m`
     - Table with initial effective height per species. 
   * - Species\_RootMass\_table
     - Variable table
     - :math:`g m^{-3}`
     - Table with initial root mass per volume of soil for each species. 


Initial vegetation states \*\*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: \*\* if ``Species_State_Variable_Input_Method`` = ``maps``
   :header-rows: 1
   :widths: 40 10 50
   :align: left

   * - Default map name (:math:`k` is the species number, starting at 0)
     - Unit
     - Description 
   * - p\_ :math:`k`.map
     - :math:`m^{2} m^{-2}`
     - fraction (<=1) of covered area (canopy cover) for :math:`k`-th species
   * - lai\_ :math:`k`.map
     - :math:`m^{2} m^{-2}`
     - Leaf area index for :math:`k`-th species. Overriden if Vegetation_dynamics=2 
   * - `Below variables are mostly for biomass allocation routines`
     - 
     - 
   * - ntr\_ :math:`k`.map 
     - :math:`trees.m^{-2}`
     - for stem density for :math:`k`-th species
   * - age\_ :math:`k`.map 
     - :math:`years`
     - initial average age for :math:`k`-th species
   * - bas\_ :math:`k`.map
     - :math:`m^{2}`
     - Total basal area for :math:`k`-th species
   * - hgt\_ :math:`k`.map
     - :math:`m`
     - effective height for :math:`k`-th species
   * - root\_ :math:`k`.map
     - :math:`g m^{-3}`
     - root mass per volume of soil for :math:`k`-th species.


Simulation outputs: map report switches
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table::
   :header-rows: 1
   :widths: 25 10 55 10
   :align: left

   * - Keyword
     - Unit
     - Description
     - File root
   * - *Inputs*
     - 
     - 
     - 
   * - Report_Long_Rad_Down
     - :math:`W m^{-2}`
     - Downwelling long wave (infrared) radiation at the top of the canopy (climate input)
     - LDown
   * - Report_Short_Rad_Down
     - :math:`W m^{-2}`
     - Incoming shortwave (visible) radiation at the top of the canopy (climate input)
     - Sdown
   * - Report_Precip
     - :math:`m~s^{-1}`
     - Input precipitation
     - Pp
   * - Report_Precip
     - :math:`m~s^{-1}`
     - Input irrigation (only if Irrigation_Input = 1)
     - Irg
   * - Report_Rel_Humidity
     - :math:`Pa^{1}~Pa^{-1}`
     - Relative humidity in the atmosphere (climate input)
     - RH
   * - Report_Wind_Speed
     - :math:`m~s^{-1}`
     - Horizontal wind speed (climate input)
     - WndSp
   * - Report_AvgAir_Temperature
     - :math:`^{\circ}C`
     - Average air temperature (climate input)
     - Tp
   * - Report_MinAir_Temperature
     - :math:`^{\circ}C`
     - Minimum air temperature (climate input)
     - TpMin
   * - Report_MaxAir_Temperature
     - :math:`^{\circ}C`
     - Maximum air temperature (climate input)
     - TpMax
   * - *Water storage*
     -
     -
     -
   * - Report_Canopy_Water_Stor_sum
     - :math:`m`
     - Intercepted water storage (summed over species fractions)
     - Cs
   * - Report_Canopy_Water_Stor
     - :math:`m`
     - Intercepted water storage of species :math:`k`
     - Cs\_ :math:`k`
   * - Report_SWE 
     - :math:`m`
     - Snow water equivalent
     - SWE
   * - Report_Saturation_Area
     - :math:`-`
     - Fraction of water-saturared area
     - SatA
   * - Report_Ponding
     - :math:`m`
     - Surface (non-channel) water height
     - PndS
   * - Report_Channel_Storage
     - :math:`m`
     - Channel water height
     - ChnS
   * - Report_Soil_Water_Content_Average
     - :math:`m^{3}~m^{-3}`
     - Average volumetric water content for entire subsurface domain
     - SWCav
   * - Report_Soil_Water_Content_Up
     - :math:`m^{3}~m^{-3}`
     - Average volumetric water content for the two upper subsurface layers
     - SWCup
   * - Report_Soil_Water_Content_L1
     - :math:`m^{3}~m^{-3}`
     - Volumetric water content for topmost subsurface layer
     - SWC1
   * - Report_Soil_Water_Content_L2
     - :math:`m^{3}~m^{-3}`
     - Volumetric water content for second subsurface layer 
     - SWC2   
   * - Report_Soil_Water_Content_L3
     - :math:`m^{3}~m^{-3}`
     - Volumetric water content for third subsurface layer
     - SWC3
   * - Report_WaterTableDepth
     - :math:`m`
     - Depth to the equivalent water table using the vertically-averaged water content
     - WTD
   * - Report_WaterTableDepth_Perched
     - :math:`m`
     - Depth to the equivalent water table, considering the shallowest saturated storage in layers
     - WTDp
   * - Report_Soil_Sat_Deficit
     - :math:`m`
     - Meters of water needed to saturate soil
     - SatDef
   * - Report_Ground_Water
     - :math:`m`
     - Meters of water above field capacity all layers combined
     - GW
   * - *Time constant*
     -
     -
     -
   * - Report_Field_Capacity_L1
     - :math:`m^3~m^{-3}`
     - Field capacity in hydrological layer 1 (if Porosity_profile > 0) or in the whole subsurface (if Porosity_profile = 0)
     - FCap1 or FCap
   * - Report_Field_Capacity_L2
     - :math:`m^3~m^{-3}`
     - Field capacity in hydrological layer 2 (if Porosity_profile > 0)
     - FCap2
   * - Report_Field_Capacity_L3
     - :math:`m^3~m^{-3}`
     - Field capacity in hydrological layer 3 (if Porosity_profile > 0)
     - FCap3
   * - *Hydro fluxes*
     -
     -
     -
   * - Report_Streamflow
     - :math:`m^{3}~s^{-1}`
     - Channel discharge
     - Q
   * - Report_Total_ET
     - :math:`m~s^{-1}`
     - Total evapotranspiration
     - Evap
   * - Report_Transpiration_sum
     - :math:`m~s^{-1}`
     - Transpiration integrated over the grid cell using species cover fractions
     - EvapT
   * - Report_Transpiration_Layer1
     - :math:`m~s^{-1}`
     - Transpiration integrated using species cover fractions, from uptake in layer 1
     - EtL1
   * - Report_Transpiration_Layer2
     - :math:`m~s^{-1}`
     - Transpiration integrated using species cover fractions, from uptake in layer 2
     - EtL2
   * - Report_Transpiration_Layer3
     - :math:`m~s^{-1}`
     - Transpiration integrated using species cover fractions, from uptake in layer 3
     - EtL3
   * - Report_Einterception_sum
     - :math:`m~s^{-1}`
     - Evaporation of intercepted water integrated over the grid cell using species fractions
     - EvapI
   * - Report_Esoil_sum
     - :math:`m~s^{-1}`
     - Soil evaporation integrated over subcanopy and bare soil fractions
     - EvapS
   * - Report_species_ET
     - :math:`m~s^{-1}`
     - Evapotranspiration for species :math:`k`
     - ETc\ :math:`k`\_
   * - Report_Transpiration
     - :math:`m~s^{-1}`
     - Transpiration from species :math:`k`
     - Et\ :math:`k`\_
   * - Report_Einterception
     - :math:`m~s^{-1}`
     - Evaporation of intercepted water for the species :math:`k`
     - Ei\ :math:`k`\_
   * - Report_Esoil
     - :math:`m~s^{-1}`
     - Soil evaporation under the species :math:`k`
     - Es\ :math:`k`\_
   * - Report_Leakage_Out_of_System
     - :math:`m~s^{-1}`
     - Vertical drainage at the bottom of the subsurface domain
     - Leak
   * - *Hydro fluxes*
     - *(internal)*
     -
     -
   * - Report_Throughfall
     - :math:`m~s^{-1}`
     - Throughfall summed over speceis fractions
     - ThFall
   * - Report_Snowmelt
     - :math:`m~s^{-1}`
     - Snow melt rate
     - Melt
   * - Report_Infiltration
     - :math:`m~s^{-1}`
     - Water (re)infiltrated water in the first hydrological layer 
     - Inf
   * - Report_Percolation_to_Layer2
     - :math:`m~s^{-1}`
     - Water transfer from the 1st to the 2nd hydrological layer
     - PrcL2
   * - Report_Percolation_to_Layer3
     - :math:`m~s^{-1}`
     - Water transfer from the 2nd to the 3rd hydrological layer
     - PrcL3
   * - Report_Groundwater_Recharge
     - :math:`m~s^{-1}`
     - Incoming water in the saturated part (above field capacity) in all layers
     - Rchg
   * - Report_GW_to_Channnel
     - :math:`m~s^{-1}`
     - Groundwater seepage rate in stream water
     - GWChn
   * - Report_Surface_to_Channel
     - :math:`m~s^{-1}`
     - Surface runoff contributing to stream water
     - SrfChn
   * - Report_Return_Flow_Surface
     - :math:`m~s^{-1}`
     - Exfiltration rate from the first hydrological layer
     - RSrf
   * - Report_Return_Flow_to_Layer1
     - :math:`m~s^{-1}`
     - Exfiltration rate from the 2nd to the 1st hydrological layer
     - RL1
   * - Report_Return_Flow_to_Layer2
     - :math:`m~s^{-1}`
     - Exfiltration rate from the 3rd to the 2nd hydrological layer
     - RL2
   * - Report_Overland_Inflow
     - :math:`m~s^{-1}`
     - Surface run-on (excluding channel inflow)
     - LSrfi 
   * - Report_Stream_Inflow
     - :math:`m~s^{-1}`
     - Incoming stream water
     - LChni
   * - Report_Groundwater_Inflow
     - :math:`m~s^{-1}`
     - Lateral groundwater inflow
     - LGWi 
   * - Report_Overland_Outflow
     - :math:`m~s^{-1}`
     - Surface run-off (excluding channel outflow)
     - LSrfo
   * - Report_Stream_Outflow
     - :math:`m`
     - Outgoing stream water
     - LChno
   * - Report_Groundwater_Outflow
     - :math:`m~s^{-1}`
     - Lateral groundwater outflow
     - LGWo
   * - *Hydro fluxes (time-cumulative)*
     - 
     -
     -
   * - Report_Transpiration_acc
     - :math:`m`
     - Time-cumulative, species-summed transpiration
     - EtA
   * - Report_Transpiration_Layer1_acc
     - :math:`m`
     - Time-cumulative, species-summed transpiration from uptake in layer 1
     - EtL1A
   * - Report_Transpiration_Layer2_acc
     - :math:`m`
     - Time-cumulative, species-summed transpiration from uptake in layer 2
     - EtL2A
   * - Report_Transpiration_Layer3_acc
     - :math:`m`
     - Time-cumulative, species-summed transpiration from uptake in layer 3
     - EtL3A
   * - Report_E_Interception_acc
     - :math:`m`
     - Time-cumulative, species-summed evaporation of intercepted water
     - EiA
   * - Report_Soil_Evaporation_acc
     - :math:`m`
     - Time-cumulative soil evaporation over subcanopy and bare soil fractions
     - EsA
   * - Report_Leakage_Out_of_System_acc
     - :math:`m`
     - Time-cumulative vertical drainage at the bottom of the subsurface domain
     - LeakA
   * - Report_Infiltration_acc
     - :math:`m`
     - Time-cumulative water (re)infiltrated water in the first hydrological layer 
     - InfA
   * - Report_Percolation_to_Layer2_acc
     - :math:`m`
     - Time-cumulative water transfer from the 1st to the 2nd hydrological layer
     - PrcL2A
   * - Report_Percolation_to_Layer3_acc
     - :math:`m`
     - Time-cumulative water transfer from the 2nd to the 3rd hydrological layer
     - PrcL3A
   * - Report_GW_to_Channnel_acc
     - :math:`m`
     - Time-cumulative groundwater seepage in stream water
     - GWChnA
   * - Report_Surface_to_Channel_acc
     - :math:`m`
     - Time-cumulative surface runoff contributing to stream water
     - SrfChnA
   * - Report_Return_Flow_Surface_acc
     - :math:`m`
     - Time-cumulative exfiltration from the first hydrological layer
     - RSrfA
   * - Report_Return_Flow_to_Layer1_acc
     - :math:`m`
     - Time-cumulative exfiltration from the 2nd to the 1st hydrological layer
     - RL1A
   * - Report_Return_Flow_to_Layer2_acc
     - :math:`m`
     - Time-cumulative exfiltration from the 3rd to the 2nd hydrological layer
     - RL2A
   * - Report_Overland_Inflow_acc
     - :math:`m`
     - Time-cumulative surface run-on (excluding channel inflow)
     - LSrfiA
   * - Report_Stream_Inflow_acc
     - :math:`m`
     - Time-cumulative incoming stream water
     - LChniA
   * - Report_Groundwater_Inflow_acc
     - :math:`m`
     - Time-cumulative lateral groundwater inflow
     - LGWiA
   * - Report_Overland_Outflow_acc
     - :math:`m`
     - Time-cumulative surface run-off (excluding channel outflow)
     - LSrfoA
   * - Report_Stream_Outflow_acc
     - :math:`m`
     - Time-cumulative outgoing stream water
     - LChnoA
   * - Report_Groundwater_Outflow_acc
     - :math:`m`
     - Time-cumulative lateral groundwater outflow
     - LGWoA
   * - *Energy balance*
     -
     -
     -
   * - Report_Surface_Net_Rad
     - :math:`Wm^{-2}`
     - Soil-level net radiation integrated over the grid cell (including bare soil fraction)
     - NRs
   * - Report_Vegetation_Net_Rad
     - :math:`Wm^{-2}`
     - Net radiation at canopy level integrated over the grid cell
     - NRv
   * - Report_Total_Net_Rad
     - :math:`Wm^{-2}`
     - Overall net radiation integrated over the grid cell (including bare soil fraction)
     - NRt
   * - Report_Surface_Latent_Heat
     - :math:`Wm^{-2}`
     - Latent heat flux of soil evaporation integrated over the grid cell (including bare soil fraction)
     - LatHs
   * - Report_Vegetation_Latent_Heat
     - :math:`Wm^{-2}`
     - Latent heat flux of canopy evaporation (interception+transpiration) integrated over the grid cell
     - LatHv
   * - Report_Total_Latent_Heat
     - :math:`Wm^{-2}`
     - Latent heat flux of ET integrated over the grid cell (including bare soil fraction)
     - LatHt
   * - Report_Surface_Sensible_Heat
     - :math:`Wm^{-2}`
     - Soil-level sensible flux heat integrated over the grid cell (including bare soil fraction)
     - SenHs
   * - Report_Vegetation_Sensible_Heat
     - :math:`Wm^{-2}`
     - Sensible heat flux at canopy level integrated over the grid cell
     - SenHv
   * - Report_Total_Sensible_Heat
     - :math:`Wm^{-2}`
     - Overall sensible heat flux integrated over the grid cell (including bare soil fraction)
     - SenHt
   * - Report_Grnd_Heat
     - :math:`Wm^{-2}`
     - Ground heat flux
     - GrndH
   * - Report_Snow_Heat
     - :math:`Wm^{-2}`
     - Turbulent heat exchange with snowpack
     - SnowH
   * - Report_Soil_Temperature
     - :math:`^{\circ}C`
     - Subsurface temperature in the bottom (2nd) thermal layer
     - Ts
   * - Report_Skin_Temperature
     - :math:`^{\circ}C`
     - Soil skin temperature (top of 1st thermal layer)
     - Tskin
   * - Report_Canopy_Temp
     - :math:`^{\circ}C`
     - Canopy temperature of species :math:`k`
     - Tc\ :math:`k`\_
   * - Report_Canopy_NetR
     - :math:`W m^{-2}`
     - Canopy-level net radiation above the species :math:`k`
     - NRc\ :math:`k`\_
   * - Report_Canopy_LE_E
     - :math:`W m^{-2}`
     - Latent heat for evaporation of canopy interception for species :math:`k`
     - LEEi\ :math:`k`\_
   * - Report_Canopy_LE_T
     - :math:`W m^{-2}`
     - Transpiration latent heat for species :math:`k`
     - LETr\ :math:`k`\_
   * - Report_Canopy_Sens_Heat
     - :math:`W m^{-2}`
     - Sensible heat, canopy layer of species :math:`k`
     - Hc\ :math:`k`\_
   * - *Vegetation state*
     - 
     -
     -
   * - Report_GPP_sum
     - :math:`gC m^{-2}`
     - Timestep-cumulative gross primary production summed over species fractions
     - GPP
   * - Report_NPP
     - :math:`gC^{-1} m^{-2}`
     - Timestep-cumulative net primary production summed over species fractions
     - NPP
   * - Report_Veget_frac
     - :math:`m^{2} m^{-2}`
     - Fraction of cell covered by canopy of species :math:`k`
     - p\ :math:`k`\_
   * - Report_Stem_Density
     - :math:`stems m^{-2}`
     - Density of individuals of species :math:`k`
     - ntr\ :math:`k`\_ 
   * - Report_Leaf_Area_Index
     - :math:`m^{2} m^{-2}`
     - Leaf area index of species :math:`k`
     - lai\ :math:`k`\_
   * - Report_Stand_Age
     - :math:`years`
     - Age of stand of species :math:`k`
     - age\ :math:`k`\_ 
   * - Report_Canopy_Conductance
     - :math:`m~s^{-1}`
     - Canopy conductance for species :math:`k`
     - gc\ :math:`k`\_ 
   * - Report_GPP
     - :math:`gC m^{-2}`
     - Timestep-cumulative gross primary production for species :math:`k`
     - gpp\ :math:`k`\_  |
   * - Report_NPP
     - :math:`gC^{-1} m^{-2}`
     - Timestep-cumulative net primary production for species :math:`k`
     - npp\ :math:`k`\_
   * - Report_Basal_Area
     - :math:`m^{2}`
     - Total basal area of species :math:`k`
     - bas\ :math:`k`\_
   * - Report_Tree_Height
     - :math:`m`
     - Height of stand of species :math:`k`
     - hgt\ :math:`k`\_
   * - Report_Root_Mass
     - :math:`g m^{-3}`
     - Root mass per volume of soil species :math:`k`
     - root\ :math:`k`\_

Map mask for time series locations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: 
   :header-rows: 1
   :widths: 15 15 70
   :align: left

   * - Keyword
     - Type
     - Description
   * - TS_mask
     - Map file name
     - Map identifying cells for which state variables will be reported. Map should be zero or NaN everywhere except for target cells. A maximum of 32 cells can be reported.


Simulation outputs: time series report switches
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: Written outputs file are time series tables at cells identified in ``TS_mask`` (except for outlet discharge, see below). In the output files, the order of columns might differ from that of locations IDs specified in ``TS_mask``, please check the output files' header.
   :header-rows: 1
   :widths: 15 10 70 10
   :align: left

   * - Keyword
     - Unit
     - Description
     - File name
   * - *Inputs*
     - 
     - 
     - 
   * - Ts_Long_Rad_Down
     - :math:`W m^{-2}`
     - Downwelling long wave (infrared) radiation at the top of the canopy (climate input)
     - LDown.tab
   * - Ts_Short_Rad_Down
     - :math:`W m^{-2}`
     - Incoming shortwave (visible) radiation at the top of the canopy (climate input)
     - Sdown.tab
   * - Ts_Precip
     - :math:`m~s^{-1}`
     - Input precipitation
     - Precip.tab
   * - Ts_Precip
     - :math:`m~s^{-1}`
     - Input irrigation (only if Irrigation_Input = 1)
     - Irrig.tab
   * - Ts_Rel_Humidity
     - :math:`Pa^{1}~Pa^{-1}`
     - Relative humidity in the atmosphere (climate input)
     - RelHumid.tab
   * - Ts_Wind_Speed
     - :math:`m~s^{-1}`
     - Horizontal wind speed (climate input)
     - WindSpeed.tab
   * - Ts_AvgAir_Temperature
     - :math:`^{\circ}C`
     - Average air temperature (climate input)
     - AvgTemp.tab
   * - Ts_MinAir_Temperature
     - :math:`^{\circ}C`
     - Minimum air temperature (climate input)
     - MinTemp.tab
   * - Ts_MaxAir_Temperature
     - :math:`^{\circ}C`
     - Maximum air temperature (climate input)
     - MaxTemp.tab
   * - *Water storage*
     -
     -
     -
   * - Ts_Canopy_Water_Stor_sum
     - :math:`m`
     - Interception storage (summed over species fractions)
     - CanopyWaterStor.tab
   * - Ts_Canopy_Water_Stor
     - :math:`m`
     - Interception storage of species :math:`k`
     - CanopyWaterStor\_\ :math:`k`\.tab
   * - Ts_SWE 
     - :math:`m`
     - Snow water equivalent
     - SWE.tab
   * - Ts_Ponding
     - :math:`m`
     - Surface (non-channel) water height
     - Ponding.tab
   * - Ts_Channel_Storage
     - :math:`m`
     - Channel water height
     - ChannelStorage.tab
   * - Ts_Soil_Water_Content\_Average
     - :math:`m^{3} m^{-3}`
     - Average volumetric water content for entire subsurface domain
     - SoilMoistureAv.tab
   * - Ts_Soil_Water_Content_Up
     - :math:`m^{3} m^{-3}`
     - Average volumetric water content for the two upper subsurface layers
     - SoilMoistureUp.tab
   * - Ts_Soil_Water_Content_L1
     - :math:`m^{3} m^{-3}`
     - Volumetric water content for topmost subsurface layer
     - SoilMoistureL1.tab
   * - Ts_Soil_Water_Content_L2
     - :math:`m^{3} m^{-3}`
     - Volumetric water content for second subsurface layer 
     - SoilMoistureL2.tab  
   * - Ts_Soil_Water_Content_L3
     - :math:`m^{3}~m^{-3}`
     - Volumetric water content for third subsurface layer
     - SoilMoistureL3.tab
   * - Ts_WaterTableDepth
     - :math:`m`
     - Depth to the equivalent water table using the vertically-averaged water content
     - WaterTableDepth.tab
   * - Ts_WaterTableDepth_Perched
     - :math:`m`
     - Depth to the equivalent water table, considering the shallowest saturated storage in layers
     - WaterTableDepth_p.tab
   * - Ts_Soil_Sat_Deficit
     - :math:`m`
     - Meters of water needed to saturate soil
     - SoilSatDef.tab
   * - Ts_Ground_Water
     - :math:`m`
     - Meters of water above field capacity all layers combined
     - GroundWater.tab
   * - *Hydro fluxes*
     -
     -
     -
   * - Ts_OutletDischarge
     - :math:`m^{3} s^{-1}`
     - Stream discharge at cells where *local_drain_direc* map (*ldd*) = 5 (outlets and sinks)
     - OutletDisch.tab
   * - Ts_Streamflow
     - :math:`m^{3} s^{-1}`
     - Stream discharge (at *TS_mask* locations)
     - Streamflow.tab
   * - Ts_Total_ET
     - :math:`m~s^{-1}`
     - Total evapotranspiration
     - Evap.tab
   * - Ts_Transpiration_sum
     - :math:`m~s^{-1}`
     - Transpiration integrated over the grid cell using species cover fractions
     - EvapT.tab
   * - Ts_Transpiration_Layer1
     - :math:`m~s^{-1}`
     - Transpiration integrated using species cover fractions, from uptake in layer 1
     - EvapT_L1.tab
   * - Ts_Transpiration_Layer2
     - :math:`m~s^{-1}`
     - Transpiration integrated using species cover fractions, from uptake in layer 2
     - EvapT_L2.tab
   * - Ts_Transpiration_Layer3
     - :math:`m~s^{-1}`
     - Transpiration integrated using species cover fractions, from uptake in layer 3
     - EvapT_L3.tab
   * - Ts_Einterception_sum
     - :math:`m~s^{-1}`
     - Evaporation of intercepted water integrated over the grid cell using species fractions
     - EvapI.tab
   * - Ts_Esoil_sum
     - :math:`m~s^{-1}`
     - Soil evaporation integrated over subcanopy and bare soil fractions
     - EvapS.tab
   * - Ts_species_ET
     - :math:`m~s^{-1}`
     - Evapotranspiration for species :math:`k`
     - ETc\_ :math:`k`.tab
   * - Ts_Transpiration
     - :math:`m~s^{-1}`
     - Transpiration from species :math:`k`
     - EvapT\_ :math:`k`.tab
   * - Ts_Einterception
     - :math:`m~s^{-1}`
     - Evaporation of intercepted water for the species :math:`k`
     - EvapI\_ :math:`k`.tab
   * - Ts_Esoil
     - :math:`m~s^{-1}`
     - Soil evaporation under the species :math:`k`
     - EvapS\_ :math:`k`.tab
   * - Ts_Leakage_Out_of_System
     - :math:`m~s^{-1}`
     - Vertical drainage at the bottom of the subsurface domain
     - Leakage.tab
   * - *Hydro fluxes*
     - *(internal)*
     -
     -
   * - Ts_Throughfall
     - :math:`m~s^{-1}`
     - Throughfall summed over speceis fractions
     - Throughfall.tab
   * - Ts_Snowmelt
     - :math:`m~s^{-1}`
     - Snow melt rate
     - Snowmelt.tab
   * - Ts_Infiltration
     - :math:`m~s^{-1}`
     - Water (re)infiltrated water in the first hydrological layer 
     - Infilt.tab
   * - Ts_Percolation_to_Layer2
     - :math:`m~s^{-1}`
     - Water transfer from the 1st to the 2nd hydrological layer
     - PercolL2.tab
   * - Ts_Percolation_to_Layer3
     - :math:`m~s^{-1}`
     - Water transfer from the 2nd to the 3rd hydrological layer
     - PercolL3.tab
   * - Ts_Groundwater_Recharge
     - :math:`m~s^{-1}`
     - Incoming water in the saturated part (above field capacity) in all layers
     - Recharge.tab
   * - Ts_GW_to_Channnel
     - :math:`m~s^{-1}`
     - Groundwater seepage rate in stream water
     - GWtoChn.tab
   * - Ts_Surface_to_Channel
     - :math:`m~s^{-1}`
     - Surface runoff contributing to stream water
     - SrftoChn.tab
   * - Ts_Return_Flow_Surface
     - :math:`m~s^{-1}`
     - Exfiltration rate from the first hydrological layer
     - ReturnSrf.tab
   * - Ts_Return_Flow_to_Layer1
     - :math:`m~s^{-1}`
     - Exfiltration rate from the 2nd to the 1st hydrological layer
     - ReturnL1.tab
   * - Ts_Return_Flow_to_Layer2
     - :math:`m~s^{-1}`
     - Exfiltration rate from the 3rd to the 2nd hydrological layer
     - ReturnL2.tab
   * - Ts_Overland_Inflow
     - :math:`m~s^{-1}`
     - Surface run-on (excluding channel inflow)
     - SrfLatI.tab
   * - Ts_Stream_Inflow
     - :math:`m~s^{-1}`
     - Incoming stream water
     - ChnLatI.tab
   * - Ts_Groundwater_Inflow
     - :math:`m~s^{-1}`
     - Lateral groundwater inflow
     - GWLatI.tab
   * - Ts_Overland_Outflow
     - :math:`m~s^{-1}`
     - Surface run-off (excluding channel outflow)
     - SrfLatO.tab
   * - Ts_Stream_Outflow
     - :math:`m~s^{-1}`
     - Outgoing stream water
     - ChnLatO.tab
   * - Ts_Groundwater_Outflow
     - :math:`m~s^{-1}`
     - Lateral groundwater outflow
     - GWLatO.tab
   * - *Energy balance*
     -
     -
     -
   * - Ts_Surface_Net_Rad
     - :math:`Wm^{-2}`
     - Soil-level net radiation integrated over the grid cell (including bare soil fraction)
     - NetRad_srf.tab
   * - Ts_Vegetation_Net_Rad
     - :math:`Wm^{-2}`
     - Net radiation at canopy level integrated over the grid cell
     - NetRad_veg.tab
   * - Ts_Total_Net_Rad
     - :math:`Wm^{-2}`
     - Overall net radiation integrated over the grid cell (including bare soil fraction)
     - NetRad_tot.tab
   * - Ts_Surface_Latent_Heat
     - :math:`Wm^{-2}`
     - Latent heat flux of soil evaporation integrated over the grid cell (including bare soil fraction)
     - LatHeat_srf.tab
   * - Ts_Vegetation_Latent_Heat
     - :math:`Wm^{-2}`
     - Latent heat flux of canopy evaporation (interception+transpiration) integrated over the grid cell
     - LatHeat_veg.tab
   * - Ts_Total_Latent_Heat
     - :math:`Wm^{-2}`
     - Latent heat flux of ET integrated over the grid cell (including bare soil fraction)
     - LatHeat_tot.tab
   * - Ts_Surface_Sensible_Heat
     - :math:`Wm^{-2}`
     - Soil-level sensible flux heat integrated over the grid cell (including bare soil fraction)
     - SensHeat_srf.tab
   * - Ts_Vegetation_Sensible_Heat
     - :math:`Wm^{-2}`
     - Sensible heat flux at canopy level integrated over the grid cell
     - SensHeat_veg.tab
   * - Ts_Total_Sensible_Heat
     - :math:`Wm^{-2}`
     - Overall sensible heat flux integrated over the grid cell (including bare soil fraction)
     - SensHeat_tot.tab
   * - Ts_Grnd_Heat
     - :math:`Wm^{-2}`
     - Ground heat flux
     - GrndHeat.tab
   * - Ts_Snow_Heat
     - :math:`Wm^{-2}`
     - Turbulent heat exchange with snowpack
     - SnowHeat.tab
   * - Ts_Soil_Temperature
     - :math:`^{\circ}C`
     - Subsurface temperature in the bottom (2nd) thermal layer
     - SoilTemp.tab
   * - Ts_Skin_Temperature
     - :math:`^{\circ}C`
     - Soil skin temperature (top of 1st thermal layer)
     - SkinTemp.tab
   * - Ts_Canopy_Temp
     - :math:`^{\circ}C`
     - Canopy temperature of species :math:`k`
     - CanopyTemp\_ :math:`k`.tab
   * - Ts_Canopy_NetR
     - :math:`W m^{-2}`
     - Canopy-level net radiation above the species :math:`k`
     - NetRadC\_ :math:`k`.tab
   * - Ts_Canopy_LE_E
     - :math:`W m^{-2}`
     - Latent heat for evaporation of canopy interception for species :math:`k`
     - CanopyLatHeatEi\_ :math:`k`.tab
   * - Ts_Canopy_LE_T
     - :math:`W m^{-2}`
     - Transpiration latent heat for species :math:`k`
     - CanopyLatHeatTr\_ :math:`k`.tab
   * - Ts_Canopy_Sens_Heat
     - :math:`W m^{-2}`
     - Sensible heat, canopy layer of species :math:`k`
     - CanopySensHeat\_ :math:`k`.tab
   * - *Vegetation state*
     - 
     -
     -
   * - Ts_GPP_sum
     - :math:`gC m^{-2}`
     - Timestep-cumulative gross primary production summed over species fractions
     - GPP.tab
   * - Ts_NPP
     - :math:`gC^{-1} m^{-2}`
     - Timestep-cumulative net primary production summed over species fractions
     - NPP.tab
   * - Ts_Veget_frac
     - :math:`m^{2} m^{-2}`
     - Fraction of cell covered by canopy of species :math:`k`
     - p\_\ :math:`k`.tab
   * - Ts_Stem_Density
     - :math:`stems m^{-2}`
     - Density of individuals of species :math:`k`
     - num_of_trees\_\ :math:`k`.tab 
   * - Ts_Leaf_Area_Index
     - :math:`m^{2} m^{-2}`
     - Leaf area index of species :math:`k`
     - lai\_\ :math:`k`.tab
   * - Ts_Stand_Age
     - :math:`years`
     - Age of stand of species :math:`k`
     - age\ :math:`k`\_ 
   * - Ts_Canopy_Conductance
     - :math:`m~s^{-1}`
     - Canopy conductance for species :math:`k`
     - CanopyConduct\_\ :math:`k`.tab
   * - Ts_GPP
     - :math:`gC m^{-2}`
     - Timestep-cumulative gross primary production for species :math:`k`
     - GPP\_\ :math:`k`.tab
   * - Ts_NPP
     - :math:`gC^{-1} m^{-2}`
     - Timestep-cumulative net primary production for species :math:`k`
     - NPP\_\ :math:`k`.tab
   * - Ts_Basal_Area
     - :math:`m^{2}`
     - Total basal area of species :math:`k`
     - BasalArea\_\ :math:`k`.tab
   * - Ts_Tree_Height
     - :math:`m`
     - Height of stand of species :math:`k`
     - TreeHeight\_\ :math:`k`.tab
   * - Ts_Root_Mass
     - :math:`g m^{-3}`
     - Root mass per volume of soil species :math:`k`
     - RootMass\_\ :math:`k`.tab